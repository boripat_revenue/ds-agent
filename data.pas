unit data;

interface

uses
  SysUtils, Classes, DB, ADODB, Dialogs;

type
  Tdm = class(TDataModule)
    db: TADOConnection;
    dscConfig: TDataSource;
    tblConfig: TADOTable;
    qry: TADOQuery;
    dsc: TDataSource;
    dscNews: TDataSource;
    tblNews: TADOTable;
    dscQ: TDataSource;
    tblQ: TADOTable;
    dscQF: TDataSource;
    tblQF: TADOTable;
    tblNewsClientID: TIntegerField;
    tblNewsTerminalID: TIntegerField;
    tblNewsHeadLine: TWideStringField;
    tblNewsDesc: TWideStringField;
    tblNewsStartDate: TDateTimeField;
    tblNewsStopDate: TDateTimeField;
    tblNewsActive: TBooleanField;
    tblNewsCUser: TWideStringField;
    tblNewsCDate: TDateTimeField;
    tblQQueue_no: TWideStringField;
    tblQClientID: TIntegerField;
    tblQTerminalID: TIntegerField;
    tblQQName: TWideStringField;
    tblQDesc: TWideStringField;
    tblQStartDate: TDateTimeField;
    tblQStopDate: TDateTimeField;
    tblQActive: TBooleanField;
    tblQCUser: TWideStringField;
    tblQCDate: TDateTimeField;
    tblQFClientID: TIntegerField;
    tblQFTerminalID: TIntegerField;
    tblQFFileName: TWideStringField;
    tblQFFileType: TWideStringField;
    tblQFFileSize: TIntegerField;
    tblQFFileLocation: TWideStringField;
    tblQFActive: TBooleanField;
    tblQFCUser: TWideStringField;
    tblQFCDate: TDateTimeField;
    tblConfigID: TAutoIncField;
    tblConfigClientID: TIntegerField;
    tblConfigTerminalID: TIntegerField;
    tblConfigshow_news: TBooleanField;
    tblConfigshow_ads: TBooleanField;
    tblConfigDefault_video: TWideStringField;
    tblConfigvideo_directory: TWideStringField;
    tblConfigvideo_program: TWideStringField;
    tblConfignews_program: TWideStringField;
    tblConfighttp_server: TWideStringField;
    qryClearQ: TADOQuery;
    qryGetActiveQ: TADOQuery;
    tblQFQID: TIntegerField;
    tblConfigrefresh_time: TIntegerField;
    tblConfigftp_server: TWideStringField;
    tblConfigftp_port: TIntegerField;
    tblConfigftp_user: TWideStringField;
    tblConfigftp_pass: TWideStringField;
    qryClearQF: TADOQuery;
    qryGetActiveFile: TADOQuery;
    tblQFOrdering: TIntegerField;
    tblQFAvail: TBooleanField;
    qryClearNews: TADOQuery;
    tblNewsID: TIntegerField;
    tblQID: TIntegerField;
    tblQFID: TIntegerField;
    tblConfigftp_path: TWideStringField;
    tblConfigftp_passive: TBooleanField;
    dscPlayQ: TDataSource;
    tblPlayQ: TADOTable;
    qryClearPlay: TADOQuery;
    tblPlayQID: TIntegerField;
    tblConfigdebug_mode: TBooleanField;
    tblQFNewFileName: TWideStringField;
    tblConfigsys_pwd: TWideStringField;
    tblQRunning: TBooleanField;
    dscBanner: TDataSource;
    tblBanner: TADOTable;
    tblBannerID: TIntegerField;
    tblBannerClientID: TIntegerField;
    tblBannerTerminalID: TIntegerField;
    tblBannerFileName: TWideStringField;
    tblBannerNewFileName: TWideStringField;
    tblBannerShowType: TIntegerField;
    tblBannerShowPosition: TIntegerField;
    tblBannerFileSize: TIntegerField;
    tblBannerFileLocation: TWideStringField;
    tblBannerStartDate: TDateTimeField;
    tblBannerStopDate: TDateTimeField;
    tblBannerActive: TBooleanField;
    tblBannerCUser: TWideStringField;
    tblBannerCDate: TDateTimeField;
    qryClearBanner: TADOQuery;
    dscPlayBq1: TDataSource;
    tblPlayBq1: TADOTable;
    IntegerField1: TIntegerField;
    dscPlayBq2: TDataSource;
    tblPlayBq2: TADOTable;
    IntegerField2: TIntegerField;
    qryClearPBQ1: TADOQuery;
    qryClearPBQ2: TADOQuery;
    qryGetActiveBanner: TADOQuery;
    tblConfignews_font: TWideStringField;
    tblConfignews_size: TIntegerField;
    tblConfignews_color: TIntegerField;
    tblConfignews_isbold: TIntegerField;
    tblConfignews_isitalic: TIntegerField;
    tblConfignews_isunderline: TIntegerField;
    tblConfigbanner_mode: TIntegerField;
    tblConfigbanner_show_interval: TIntegerField;
    tblConfignews_delay: TIntegerField;
    tblConfignews_height: TIntegerField;
    tblConfigbanner_width: TIntegerField;
    tblConfignews_back_color: TIntegerField;
    tblConfignews_step: TIntegerField;
    tblConfigbanner_prop: TBooleanField;
    tblQIsMonday: TBooleanField;
    tblQIsTuesday: TBooleanField;
    tblQIsWednesday: TBooleanField;
    tblQIsThursday: TBooleanField;
    tblQIsFriday: TBooleanField;
    tblQIsSaturday: TBooleanField;
    tblQIsSunday: TBooleanField;
    tblQStartInterval: TDateTimeField;
    tblQEndInterval: TDateTimeField;
    tblConfigshow_clock: TBooleanField;
    tblConfigrss_active: TBooleanField;
    tblConfigvideo_source: TBooleanField;
    tblConfiglogo_width: TIntegerField;
    tblConfiglogo_stretch: TBooleanField;
    tblConfiglogo_ratio: TBooleanField;
    tblFiles: TADOTable;
    tblConfigdef_type: TIntegerField;
    tblConfigdef_file: TWideStringField;
    tblNewsQID: TIntegerField;
    tblQFFileHash: TWideStringField;
    tblPlayQQID: TIntegerField;
    tblBannerQID: TIntegerField;
    tblBannerFileHash: TWideStringField;
    tblPlayBq1QID: TIntegerField;
    tblPlayBq2QID: TIntegerField;
    tblFileList: TADOTable;
    tblFileListfid: TAutoIncField;
    tblFileListforg: TWideStringField;
    tblFileListfname: TWideStringField;
    tblFileListfsize: TIntegerField;
    tblFileListfupdate: TDateTimeField;
    tblFileListfaccess: TDateTimeField;
    tblQOrder: TIntegerField;
    tblPlayLog: TADOTable;
    tblPlayLogID: TAutoIncField;
    tblPlayLogqid: TIntegerField;
    tblPlayLoglog_id: TIntegerField;
    tblPlayLogtype: TWideStringField;
    tblPlayLogfile: TWideStringField;
    tblPlayLogaction: TWideStringField;
    tblPlayLogcdate: TWideStringField;
    tblConfigQID: TIntegerField;
    tblPlayLayout: TADOTable;
    tblConfightml_show: TBooleanField;
    tblConfightml_interval: TIntegerField;
    tblConfightml_pos: TWideStringField;
    tblConfightml_size: TIntegerField;
    tblPlayLayoutID: TAutoIncField;
    tblPlayLayoutClientID: TIntegerField;
    tblPlayLayoutTerminalID: TIntegerField;
    tblPlayLayoutQID: TIntegerField;
    tblPlayLayoutshow_news: TBooleanField;
    tblPlayLayoutshow_ads: TBooleanField;
    tblPlayLayoutDefault_video: TWideStringField;
    tblPlayLayoutvideo_directory: TWideStringField;
    tblPlayLayouthttp_server: TWideStringField;
    tblPlayLayoutvideo_program: TWideStringField;
    tblPlayLayoutnews_program: TWideStringField;
    tblPlayLayoutrefresh_time: TIntegerField;
    tblPlayLayoutftp_server: TWideStringField;
    tblPlayLayoutftp_port: TIntegerField;
    tblPlayLayoutftp_user: TWideStringField;
    tblPlayLayoutftp_pass: TWideStringField;
    tblPlayLayoutftp_path: TWideStringField;
    tblPlayLayoutftp_passive: TBooleanField;
    tblPlayLayoutdebug_mode: TBooleanField;
    tblPlayLayoutsys_pwd: TWideStringField;
    tblPlayLayoutnews_font: TWideStringField;
    tblPlayLayoutnews_size: TIntegerField;
    tblPlayLayoutnews_color: TIntegerField;
    tblPlayLayoutnews_isbold: TIntegerField;
    tblPlayLayoutnews_isitalic: TIntegerField;
    tblPlayLayoutnews_isunderline: TIntegerField;
    tblPlayLayoutbanner_mode: TIntegerField;
    tblPlayLayoutbanner_show_interval: TIntegerField;
    tblPlayLayoutnews_delay: TIntegerField;
    tblPlayLayoutnews_height: TIntegerField;
    tblPlayLayoutbanner_width: TIntegerField;
    tblPlayLayoutnews_back_color: TIntegerField;
    tblPlayLayoutnews_step: TIntegerField;
    tblPlayLayoutbanner_prop: TBooleanField;
    tblPlayLayoutshow_clock: TBooleanField;
    tblPlayLayoutrss_active: TBooleanField;
    tblPlayLayoutvideo_source: TBooleanField;
    tblPlayLayoutlogo_width: TIntegerField;
    tblPlayLayoutlogo_stretch: TBooleanField;
    tblPlayLayoutlogo_ratio: TBooleanField;
    tblPlayLayoutdef_type: TIntegerField;
    tblPlayLayoutdef_file: TWideStringField;
    tblPlayLayouthtml_show: TBooleanField;
    tblPlayLayouthtml_interval: TIntegerField;
    tblPlayLayouthtml_pos: TWideStringField;
    tblPlayLayouthtml_size: TIntegerField;
    tblPlayLayoutsystem_pass: TWideStringField;
    tblPlayLayouthttp_program: TWideStringField;
    tblPlayLayoutsystem_interval: TIntegerField;
    tblPlayLayoutbanner_interval: TIntegerField;
    tblPlayLayoutbanner_size: TIntegerField;
    tblPlayLayoutclock_bgcolor: TWideStringField;
    tblPlayLayoutclock_font_color: TWideStringField;
    tblPlayLayoutclock_format: TWideStringField;
    tblPlayLayoutclock_show: TBooleanField;
    tblPlayLayoutrss_interval: TIntegerField;
    tblPlayLayoutvideo_path: TWideStringField;
    tblPlayLayoutbanner_show: TBooleanField;
    tblPlayLayoutbanner_style: TIntegerField;
    tblPlayLayoutbanner_effect: TWideStringField;
    tblHTMLUrl: TADOTable;
    tblHTMLUrlCID: TIntegerField;
    tblHTMLUrlQID: TIntegerField;
    tblHTMLUrlURL: TWideStringField;
    tblHTMLUrlOrdering: TIntegerField;
    tblHTMLUrlInterval: TIntegerField;
    tblConfigrss_url: TWideStringField;
    tblPlayLayoutrss_url: TWideStringField;
    tblHTMLUrlID: TIntegerField;
    procedure tblNewsBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    function HavePipe(input: string): boolean;
    procedure RefreshPlayQ;
    procedure FindPlayQIdx(idx: integer);

    procedure RefreshPlayBQ;
    procedure FindPlayBQ1Idx(idx: integer);
    procedure FindPlayBQ2Idx(idx: integer);
  end;

var
  dm: Tdm;

implementation

uses DateUtils;

{$R *.dfm}

procedure Tdm.tblNewsBeforePost(DataSet: TDataSet);
begin
  if HavePipe(DataSet.FieldByName('headline').AsString) then
  begin
    messagedlg('������ | ������Ң���', mtError, [mbok], 0);
    abort;
  end;
end;

function Tdm.HavePipe(input: string): boolean;
begin
  result := Pos('|', input) > 0;
end;

procedure Tdm.RefreshPlayQ;
begin
  with tblPlayQ do
  begin
    if active then close;
    open;
  end;

  with tblQF do
  begin
    if active then close;
    open;
  end;
end;

procedure Tdm.RefreshPlayBQ;
begin
  with tblPlayBq1 do
  begin
    if active then close;
    open;
  end;

  with tblPlayBq2 do
  begin
    if active then close;
    open;
  end;

  with tblBanner do
  begin
    if active then close;
    open;
  end;
end;

procedure Tdm.FindPlayQIdx(idx: integer);
var
  i: integer;
begin     // start the first record call idx = 0
  with tblPlayQ do
  begin
    first;

    for i := 1 to idx do
    begin
      next;
    end;

  end;
end;

procedure Tdm.FindPlayBQ1Idx(idx: integer);
var
  i: integer;
begin     // start the first record call idx = 0
  with tblPlayBq1 do
  begin
    first;

    for i := 1 to idx do
    begin
      next;
    end;

  end;
end;

procedure Tdm.FindPlayBQ2Idx(idx: integer);
var
  i: integer;
begin     // start the first record call idx = 0
  with tblPlayBq1 do
  begin
    first;

    for i := 1 to idx do
    begin
      next;
    end;

  end;
end;

end.
