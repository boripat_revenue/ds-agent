object dm: Tdm
  OldCreateOrder = False
  Left = 286
  Top = 200
  Height = 764
  Width = 706
  object db: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Password="";User ID=Admin;Data ' +
      'Source=db.mdb;Mode=Share Deny None;Extended Properties="";Jet OL' +
      'EDB:System database="";Jet OLEDB:Registry Path="";Jet OLEDB:Data' +
      'base Password=9999;Jet OLEDB:Engine Type=5;Jet OLEDB:Database Lo' +
      'cking Mode=1;Jet OLEDB:Global Partial Bulk Ops=2;Jet OLEDB:Globa' +
      'l Bulk Transactions=1;Jet OLEDB:New Database Password="";Jet OLE' +
      'DB:Create System Database=False;Jet OLEDB:Encrypt Database=False' +
      ';Jet OLEDB:Don'#39't Copy Locale on Compact=False;Jet OLEDB:Compact ' +
      'Without Replica Repair=False;Jet OLEDB:SFP=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 40
    Top = 24
  end
  object dscConfig: TDataSource
    DataSet = tblPlayLayout
    Left = 120
    Top = 88
  end
  object tblConfig: TADOTable
    Connection = db
    CursorType = ctStatic
    TableName = 'configs'
    Left = 40
    Top = 80
    object tblConfigID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object tblConfigQID: TIntegerField
      FieldName = 'QID'
    end
    object tblConfigClientID: TIntegerField
      FieldName = 'ClientID'
    end
    object tblConfigTerminalID: TIntegerField
      FieldName = 'TerminalID'
    end
    object tblConfigshow_news: TBooleanField
      FieldName = 'show_news'
    end
    object tblConfigshow_ads: TBooleanField
      FieldName = 'show_ads'
    end
    object tblConfigDefault_video: TWideStringField
      FieldName = 'Default_video'
      Size = 100
    end
    object tblConfigvideo_directory: TWideStringField
      FieldName = 'video_directory'
      Size = 255
    end
    object tblConfighttp_server: TWideStringField
      FieldName = 'http_server'
      Size = 100
    end
    object tblConfigvideo_program: TWideStringField
      FieldName = 'video_program'
      Size = 255
    end
    object tblConfignews_program: TWideStringField
      FieldName = 'news_program'
      Size = 255
    end
    object tblConfigrefresh_time: TIntegerField
      FieldName = 'refresh_time'
    end
    object tblConfigftp_server: TWideStringField
      FieldName = 'ftp_server'
      Size = 200
    end
    object tblConfigftp_port: TIntegerField
      FieldName = 'ftp_port'
    end
    object tblConfigftp_user: TWideStringField
      FieldName = 'ftp_user'
      Size = 50
    end
    object tblConfigftp_pass: TWideStringField
      FieldName = 'ftp_pass'
      Size = 50
    end
    object tblConfigftp_path: TWideStringField
      FieldName = 'ftp_path'
      Size = 100
    end
    object tblConfigftp_passive: TBooleanField
      FieldName = 'ftp_passive'
    end
    object tblConfigdebug_mode: TBooleanField
      FieldName = 'debug_mode'
    end
    object tblConfigsys_pwd: TWideStringField
      FieldName = 'sys_pwd'
      Size = 255
    end
    object tblConfignews_font: TWideStringField
      FieldName = 'news_font'
      Size = 255
    end
    object tblConfignews_size: TIntegerField
      FieldName = 'news_size'
    end
    object tblConfignews_color: TIntegerField
      FieldName = 'news_color'
    end
    object tblConfignews_isbold: TIntegerField
      FieldName = 'news_isbold'
    end
    object tblConfignews_isitalic: TIntegerField
      FieldName = 'news_isitalic'
    end
    object tblConfignews_isunderline: TIntegerField
      FieldName = 'news_isunderline'
    end
    object tblConfigbanner_mode: TIntegerField
      FieldName = 'banner_mode'
    end
    object tblConfigbanner_show_interval: TIntegerField
      FieldName = 'banner_show_interval'
    end
    object tblConfignews_delay: TIntegerField
      FieldName = 'news_delay'
    end
    object tblConfignews_height: TIntegerField
      FieldName = 'news_height'
    end
    object tblConfigbanner_width: TIntegerField
      FieldName = 'banner_width'
    end
    object tblConfignews_back_color: TIntegerField
      FieldName = 'news_back_color'
    end
    object tblConfignews_step: TIntegerField
      FieldName = 'news_step'
    end
    object tblConfigbanner_prop: TBooleanField
      FieldName = 'banner_prop'
    end
    object tblConfigshow_clock: TBooleanField
      FieldName = 'show_clock'
    end
    object tblConfigrss_active: TBooleanField
      FieldName = 'rss_active'
    end
    object tblConfigrss_url: TWideStringField
      FieldName = 'rss_url'
      Size = 255
    end
    object tblConfigvideo_source: TBooleanField
      FieldName = 'video_source'
    end
    object tblConfiglogo_width: TIntegerField
      FieldName = 'logo_width'
    end
    object tblConfiglogo_stretch: TBooleanField
      FieldName = 'logo_stretch'
    end
    object tblConfiglogo_ratio: TBooleanField
      FieldName = 'logo_ratio'
    end
    object tblConfigdef_type: TIntegerField
      FieldName = 'def_type'
    end
    object tblConfigdef_file: TWideStringField
      FieldName = 'def_file'
      Size = 128
    end
    object tblConfightml_show: TBooleanField
      FieldName = 'html_show'
    end
    object tblConfightml_interval: TIntegerField
      FieldName = 'html_interval'
    end
    object tblConfightml_pos: TWideStringField
      FieldName = 'html_pos'
      Size = 50
    end
    object tblConfightml_size: TIntegerField
      FieldName = 'html_size'
    end
  end
  object qry: TADOQuery
    Connection = db
    CursorType = ctStatic
    Parameters = <>
    Left = 360
    Top = 24
  end
  object dsc: TDataSource
    DataSet = qry
    Left = 408
    Top = 24
  end
  object dscNews: TDataSource
    DataSet = tblNews
    Left = 120
    Top = 152
  end
  object tblNews: TADOTable
    Connection = db
    CursorType = ctStatic
    BeforePost = tblNewsBeforePost
    TableName = 'news'
    Left = 40
    Top = 152
    object tblNewsID: TIntegerField
      FieldName = 'ID'
    end
    object tblNewsClientID: TIntegerField
      FieldName = 'ClientID'
    end
    object tblNewsTerminalID: TIntegerField
      FieldName = 'TerminalID'
    end
    object tblNewsHeadLine: TWideStringField
      FieldName = 'HeadLine'
      Size = 255
    end
    object tblNewsDesc: TWideStringField
      FieldName = 'Desc'
      Size = 255
    end
    object tblNewsStartDate: TDateTimeField
      FieldName = 'StartDate'
    end
    object tblNewsStopDate: TDateTimeField
      FieldName = 'StopDate'
    end
    object tblNewsActive: TBooleanField
      FieldName = 'Active'
    end
    object tblNewsCUser: TWideStringField
      FieldName = 'CUser'
      Size = 50
    end
    object tblNewsCDate: TDateTimeField
      FieldName = 'CDate'
    end
    object tblNewsQID: TIntegerField
      FieldName = 'QID'
    end
  end
  object dscQ: TDataSource
    DataSet = tblQ
    Left = 120
    Top = 216
  end
  object tblQ: TADOTable
    Connection = db
    CursorType = ctStatic
    TableName = 'queue'
    Left = 40
    Top = 216
    object tblQID: TIntegerField
      FieldName = 'ID'
    end
    object tblQQueue_no: TWideStringField
      FieldName = 'Queue_no'
      Size = 255
    end
    object tblQClientID: TIntegerField
      FieldName = 'ClientID'
    end
    object tblQTerminalID: TIntegerField
      FieldName = 'TerminalID'
    end
    object tblQQName: TWideStringField
      FieldName = 'QName'
      Size = 255
    end
    object tblQDesc: TWideStringField
      FieldName = 'Desc'
      Size = 255
    end
    object tblQStartDate: TDateTimeField
      FieldName = 'StartDate'
    end
    object tblQStopDate: TDateTimeField
      FieldName = 'StopDate'
    end
    object tblQActive: TBooleanField
      FieldName = 'Active'
    end
    object tblQCUser: TWideStringField
      FieldName = 'CUser'
      Size = 50
    end
    object tblQCDate: TDateTimeField
      FieldName = 'CDate'
    end
    object tblQRunning: TBooleanField
      FieldName = 'Running'
    end
    object tblQIsMonday: TBooleanField
      FieldName = 'IsMonday'
    end
    object tblQIsTuesday: TBooleanField
      FieldName = 'IsTuesday'
    end
    object tblQIsWednesday: TBooleanField
      FieldName = 'IsWednesday'
    end
    object tblQIsThursday: TBooleanField
      FieldName = 'IsThursday'
    end
    object tblQIsFriday: TBooleanField
      FieldName = 'IsFriday'
    end
    object tblQIsSaturday: TBooleanField
      FieldName = 'IsSaturday'
    end
    object tblQIsSunday: TBooleanField
      FieldName = 'IsSunday'
    end
    object tblQStartInterval: TDateTimeField
      FieldName = 'StartInterval'
    end
    object tblQEndInterval: TDateTimeField
      FieldName = 'EndInterval'
    end
    object tblQOrder: TIntegerField
      FieldName = 'Order'
    end
  end
  object dscQF: TDataSource
    DataSet = tblQF
    Left = 120
    Top = 280
  end
  object tblQF: TADOTable
    Connection = db
    CursorType = ctStatic
    IndexFieldNames = 'QID'
    MasterFields = 'ID'
    MasterSource = dscQ
    TableName = 'queue_files'
    Left = 40
    Top = 280
    object tblQFID: TIntegerField
      FieldName = 'ID'
    end
    object tblQFQID: TIntegerField
      FieldName = 'QID'
    end
    object tblQFClientID: TIntegerField
      FieldName = 'ClientID'
    end
    object tblQFTerminalID: TIntegerField
      FieldName = 'TerminalID'
    end
    object tblQFFileName: TWideStringField
      FieldName = 'FileName'
      Size = 255
    end
    object tblQFNewFileName: TWideStringField
      FieldName = 'NewFileName'
      Size = 255
    end
    object tblQFFileType: TWideStringField
      FieldName = 'FileType'
      Size = 255
    end
    object tblQFFileSize: TIntegerField
      FieldName = 'FileSize'
    end
    object tblQFFileLocation: TWideStringField
      FieldName = 'FileLocation'
      Size = 255
    end
    object tblQFActive: TBooleanField
      FieldName = 'Active'
    end
    object tblQFAvail: TBooleanField
      FieldName = 'Avail'
    end
    object tblQFOrdering: TIntegerField
      FieldName = 'Ordering'
    end
    object tblQFCUser: TWideStringField
      FieldName = 'CUser'
      Size = 50
    end
    object tblQFCDate: TDateTimeField
      FieldName = 'CDate'
    end
    object tblQFFileHash: TWideStringField
      FieldName = 'FileHash'
      Size = 50
    end
  end
  object qryClearQ: TADOQuery
    Connection = db
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'delete from queue')
    Left = 208
    Top = 216
  end
  object qryGetActiveQ: TADOQuery
    Connection = db
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select id, queue_no'
      'from queue'
      'where running')
    Left = 280
    Top = 216
  end
  object qryClearQF: TADOQuery
    Connection = db
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'delete from queue_files')
    Left = 208
    Top = 280
  end
  object qryGetActiveFile: TADOQuery
    Connection = db
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select qf.id,qf.qid'
      'from queue q, queue_files qf'
      'where q.id = qf.qid'
      'and q.running'
      'and qf.active'
      'order by ordering')
    Left = 280
    Top = 280
  end
  object qryClearNews: TADOQuery
    Connection = db
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'delete from news')
    Left = 208
    Top = 152
  end
  object dscPlayQ: TDataSource
    DataSet = tblPlayQ
    Left = 120
    Top = 352
  end
  object tblPlayQ: TADOTable
    Connection = db
    CursorType = ctStatic
    TableName = 'playq'
    Left = 40
    Top = 352
    object tblPlayQID: TIntegerField
      FieldName = 'ID'
    end
    object tblPlayQQID: TIntegerField
      FieldName = 'QID'
    end
  end
  object qryClearPlay: TADOQuery
    Connection = db
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'delete from playq')
    Left = 208
    Top = 352
  end
  object dscBanner: TDataSource
    DataSet = tblBanner
    Left = 120
    Top = 432
  end
  object tblBanner: TADOTable
    Connection = db
    CursorType = ctStatic
    IndexFieldNames = 'id'
    TableName = 'banner'
    Left = 40
    Top = 432
    object tblBannerID: TIntegerField
      FieldName = 'ID'
    end
    object tblBannerClientID: TIntegerField
      FieldName = 'ClientID'
    end
    object tblBannerTerminalID: TIntegerField
      FieldName = 'TerminalID'
    end
    object tblBannerFileName: TWideStringField
      FieldName = 'FileName'
      Size = 255
    end
    object tblBannerNewFileName: TWideStringField
      FieldName = 'NewFileName'
      Size = 255
    end
    object tblBannerShowType: TIntegerField
      FieldName = 'ShowType'
    end
    object tblBannerShowPosition: TIntegerField
      FieldName = 'ShowPosition'
    end
    object tblBannerFileSize: TIntegerField
      FieldName = 'FileSize'
    end
    object tblBannerFileLocation: TWideStringField
      FieldName = 'FileLocation'
      Size = 255
    end
    object tblBannerStartDate: TDateTimeField
      FieldName = 'StartDate'
    end
    object tblBannerStopDate: TDateTimeField
      FieldName = 'StopDate'
    end
    object tblBannerActive: TBooleanField
      FieldName = 'Active'
    end
    object tblBannerCUser: TWideStringField
      FieldName = 'CUser'
      Size = 50
    end
    object tblBannerCDate: TDateTimeField
      FieldName = 'CDate'
    end
    object tblBannerQID: TIntegerField
      FieldName = 'QID'
    end
    object tblBannerFileHash: TWideStringField
      FieldName = 'FileHash'
      Size = 50
    end
  end
  object qryClearBanner: TADOQuery
    Connection = db
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'delete from banner')
    Left = 208
    Top = 432
  end
  object dscPlayBq1: TDataSource
    DataSet = tblPlayBq1
    Left = 400
    Top = 408
  end
  object tblPlayBq1: TADOTable
    Connection = db
    CursorType = ctStatic
    TableName = 'playbq1'
    Left = 320
    Top = 408
    object IntegerField1: TIntegerField
      FieldName = 'ID'
    end
    object tblPlayBq1QID: TIntegerField
      FieldName = 'QID'
    end
  end
  object dscPlayBq2: TDataSource
    DataSet = tblPlayBq2
    Left = 400
    Top = 472
  end
  object tblPlayBq2: TADOTable
    Connection = db
    CursorType = ctStatic
    TableName = 'playbq2'
    Left = 320
    Top = 472
    object IntegerField2: TIntegerField
      FieldName = 'ID'
    end
    object tblPlayBq2QID: TIntegerField
      FieldName = 'QID'
    end
  end
  object qryClearPBQ1: TADOQuery
    Connection = db
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'delete from playbq1')
    Left = 480
    Top = 408
  end
  object qryClearPBQ2: TADOQuery
    Connection = db
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'delete from playbq2')
    Left = 480
    Top = 472
  end
  object qryGetActiveBanner: TADOQuery
    Connection = db
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'stype'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'sposition'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      'select id'
      'from banner'
      'where active '
      'and startdate <= now'
      'and stopdate >= now'
      'and showtype = :stype'
      'and showposition = :sposition')
    Left = 592
    Top = 408
  end
  object tblFiles: TADOTable
    Connection = db
    TableName = 'queue_files'
    Left = 208
    Top = 88
  end
  object tblFileList: TADOTable
    Connection = db
    CursorType = ctStatic
    TableName = 'filelist'
    Left = 40
    Top = 504
    object tblFileListfid: TAutoIncField
      FieldName = 'fid'
      ReadOnly = True
    end
    object tblFileListforg: TWideStringField
      FieldName = 'forg'
      Size = 255
    end
    object tblFileListfname: TWideStringField
      FieldName = 'fname'
      Size = 32
    end
    object tblFileListfsize: TIntegerField
      FieldName = 'fsize'
    end
    object tblFileListfupdate: TDateTimeField
      FieldName = 'fupdate'
    end
    object tblFileListfaccess: TDateTimeField
      FieldName = 'faccess'
    end
  end
  object tblPlayLog: TADOTable
    Connection = db
    TableName = 'log_play'
    Left = 128
    Top = 504
    object tblPlayLogID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object tblPlayLogqid: TIntegerField
      FieldName = 'qid'
    end
    object tblPlayLoglog_id: TIntegerField
      FieldName = 'log_id'
    end
    object tblPlayLogtype: TWideStringField
      FieldName = 'type'
      Size = 50
    end
    object tblPlayLogfile: TWideStringField
      FieldName = 'file'
      Size = 50
    end
    object tblPlayLogaction: TWideStringField
      FieldName = 'action'
      Size = 50
    end
    object tblPlayLogcdate: TWideStringField
      FieldName = 'cdate'
      Size = 50
    end
  end
  object tblPlayLayout: TADOTable
    Connection = db
    IndexFieldNames = 'QID'
    TableName = 'QryPlayLayout'
    Left = 240
    Top = 520
    object tblPlayLayoutID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object tblPlayLayoutClientID: TIntegerField
      FieldName = 'ClientID'
    end
    object tblPlayLayoutTerminalID: TIntegerField
      FieldName = 'TerminalID'
    end
    object tblPlayLayoutQID: TIntegerField
      FieldName = 'QID'
    end
    object tblPlayLayoutshow_news: TBooleanField
      FieldName = 'show_news'
    end
    object tblPlayLayoutshow_ads: TBooleanField
      FieldName = 'show_ads'
    end
    object tblPlayLayoutDefault_video: TWideStringField
      FieldName = 'Default_video'
      Size = 100
    end
    object tblPlayLayoutvideo_directory: TWideStringField
      FieldName = 'video_directory'
      Size = 255
    end
    object tblPlayLayouthttp_server: TWideStringField
      FieldName = 'http_server'
      Size = 100
    end
    object tblPlayLayoutvideo_program: TWideStringField
      FieldName = 'video_program'
      Size = 50
    end
    object tblPlayLayoutnews_program: TWideStringField
      FieldName = 'news_program'
      Size = 50
    end
    object tblPlayLayoutrefresh_time: TIntegerField
      FieldName = 'refresh_time'
    end
    object tblPlayLayoutftp_server: TWideStringField
      FieldName = 'ftp_server'
      Size = 200
    end
    object tblPlayLayoutftp_port: TIntegerField
      FieldName = 'ftp_port'
    end
    object tblPlayLayoutftp_user: TWideStringField
      FieldName = 'ftp_user'
      Size = 50
    end
    object tblPlayLayoutftp_pass: TWideStringField
      FieldName = 'ftp_pass'
      Size = 50
    end
    object tblPlayLayoutftp_path: TWideStringField
      FieldName = 'ftp_path'
      Size = 100
    end
    object tblPlayLayoutftp_passive: TBooleanField
      FieldName = 'ftp_passive'
    end
    object tblPlayLayoutdebug_mode: TBooleanField
      FieldName = 'debug_mode'
    end
    object tblPlayLayoutsys_pwd: TWideStringField
      FieldName = 'sys_pwd'
      Size = 255
    end
    object tblPlayLayoutnews_font: TWideStringField
      FieldName = 'news_font'
      Size = 255
    end
    object tblPlayLayoutnews_size: TIntegerField
      FieldName = 'news_size'
    end
    object tblPlayLayoutnews_color: TIntegerField
      FieldName = 'news_color'
    end
    object tblPlayLayoutnews_isbold: TIntegerField
      FieldName = 'news_isbold'
    end
    object tblPlayLayoutnews_isitalic: TIntegerField
      FieldName = 'news_isitalic'
    end
    object tblPlayLayoutnews_isunderline: TIntegerField
      FieldName = 'news_isunderline'
    end
    object tblPlayLayoutbanner_mode: TIntegerField
      FieldName = 'banner_mode'
    end
    object tblPlayLayoutbanner_show_interval: TIntegerField
      FieldName = 'banner_show_interval'
    end
    object tblPlayLayoutnews_delay: TIntegerField
      FieldName = 'news_delay'
    end
    object tblPlayLayoutnews_height: TIntegerField
      FieldName = 'news_height'
    end
    object tblPlayLayoutbanner_width: TIntegerField
      FieldName = 'banner_width'
    end
    object tblPlayLayoutnews_back_color: TIntegerField
      FieldName = 'news_back_color'
    end
    object tblPlayLayoutnews_step: TIntegerField
      FieldName = 'news_step'
    end
    object tblPlayLayoutbanner_prop: TBooleanField
      FieldName = 'banner_prop'
    end
    object tblPlayLayoutshow_clock: TBooleanField
      FieldName = 'show_clock'
    end
    object tblPlayLayoutrss_active: TBooleanField
      FieldName = 'rss_active'
    end
    object tblPlayLayoutrss_url: TWideStringField
      FieldName = 'rss_url'
      Size = 255
    end
    object tblPlayLayoutvideo_source: TBooleanField
      FieldName = 'video_source'
    end
    object tblPlayLayoutlogo_width: TIntegerField
      FieldName = 'logo_width'
    end
    object tblPlayLayoutlogo_stretch: TBooleanField
      FieldName = 'logo_stretch'
    end
    object tblPlayLayoutlogo_ratio: TBooleanField
      FieldName = 'logo_ratio'
    end
    object tblPlayLayoutdef_type: TIntegerField
      FieldName = 'def_type'
    end
    object tblPlayLayoutdef_file: TWideStringField
      FieldName = 'def_file'
      Size = 128
    end
    object tblPlayLayouthtml_show: TBooleanField
      FieldName = 'html_show'
    end
    object tblPlayLayouthtml_interval: TIntegerField
      FieldName = 'html_interval'
    end
    object tblPlayLayouthtml_pos: TWideStringField
      FieldName = 'html_pos'
      Size = 50
    end
    object tblPlayLayouthtml_size: TIntegerField
      FieldName = 'html_size'
    end
    object tblPlayLayoutsystem_pass: TWideStringField
      FieldName = 'system_pass'
      Size = 50
    end
    object tblPlayLayouthttp_program: TWideStringField
      FieldName = 'http_program'
      Size = 50
    end
    object tblPlayLayoutsystem_interval: TIntegerField
      FieldName = 'system_interval'
    end
    object tblPlayLayoutbanner_interval: TIntegerField
      FieldName = 'banner_interval'
    end
    object tblPlayLayoutbanner_size: TIntegerField
      FieldName = 'banner_size'
    end
    object tblPlayLayoutclock_bgcolor: TWideStringField
      FieldName = 'clock_bgcolor'
      Size = 50
    end
    object tblPlayLayoutclock_font_color: TWideStringField
      FieldName = 'clock_font_color'
      Size = 50
    end
    object tblPlayLayoutclock_format: TWideStringField
      FieldName = 'clock_format'
      Size = 50
    end
    object tblPlayLayoutclock_show: TBooleanField
      FieldName = 'clock_show'
    end
    object tblPlayLayoutrss_interval: TIntegerField
      FieldName = 'rss_interval'
    end
    object tblPlayLayoutvideo_path: TWideStringField
      FieldName = 'video_path'
      Size = 50
    end
    object tblPlayLayoutbanner_show: TBooleanField
      FieldName = 'banner_show'
    end
    object tblPlayLayoutbanner_style: TIntegerField
      FieldName = 'banner_style'
    end
    object tblPlayLayoutbanner_effect: TWideStringField
      FieldName = 'banner_effect'
      Size = 50
    end
  end
  object tblHTMLUrl: TADOTable
    Connection = db
    TableName = 'html_url'
    Left = 40
    Top = 568
    object tblHTMLUrlID: TIntegerField
      FieldName = 'ID'
    end
    object tblHTMLUrlCID: TIntegerField
      FieldName = 'CID'
    end
    object tblHTMLUrlQID: TIntegerField
      FieldName = 'QID'
    end
    object tblHTMLUrlURL: TWideStringField
      FieldName = 'URL'
      Size = 255
    end
    object tblHTMLUrlOrdering: TIntegerField
      FieldName = 'Ordering'
    end
    object tblHTMLUrlInterval: TIntegerField
      FieldName = 'Interval'
    end
  end
end
