program DSagn;

uses
  Forms,
  data in 'data.pas' {dm: TDataModule},
  fmLog in 'fmLog.pas' {frmLog},
  fmMain in 'fmMain.pas' {frmMain};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'DS Agent';
  Application.ShowMainForm := false;
  Application.CreateForm(Tdm, dm);
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmLog, frmLog);
  Application.Run;
end.
