unit fmMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Comobj, Dialogs, Menus, RXShell, ExtCtrls, AdvSysKeyboardHook, HotKeyManager,
  StdCtrls,INIfiles;

type
  TfrmMain = class(TForm)
    RxTrayIcon1: TRxTrayIcon;
    PopupMenu1: TPopupMenu;
    ShowLog1: TMenuItem;
    Exit1: TMenuItem;
    Timer1: TTimer;
    TStart: TTimer;
    KeyHook: THotKeyManager;
    txtSerial: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ShowLog1Click(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure HideLog1Click(Sender: TObject);
    procedure TStartTimer(Sender: TObject);
    procedure RxTrayIcon1DblClick(Sender: TObject);
    procedure KeyHookHotKeyPressed(HotKey: Cardinal; Index: Word);
  private
    { Private declarations }
  public
    { Public declarations }
    function CompactAndRepair: Boolean;
    procedure CompactDB();
    procedure ReadConfig();
    procedure CreateURLConfig();
  end;

const
  cProduct='InfoExpress-Lite';
  cKeyMonitor='Monitor';
  cKeyPlayer='Player';
  cKeyAgent='Agent';
  cValAgent='2.4.1';
  cMaxSendLog=200;
var
  frmMain: TfrmMain;

  sProduct:string;
  sComputerID: string;
  sRegComputerID: string;
  sRegName01,sRegName02,sRegName03,sRegName04: string;
  pData01,pData02: pChar;
  sShow,sInputKey: string;
  sData01,sData02: string;
  sTmp01,sTmp02: string;
  iPlayerLeft,iPlayerTop,iPlayerWidth,iPlayerHeight: integer;
  bPlayerRatio,bFullScreen: boolean;
  sValMonitor,sValAgent,sValPlayer:string;
  arrURL  : Array of string;
  bHTMLShow: boolean; // Keep data from INI
  iHTMLSize: integer;
  iHTMLItem: integer;
  iHTMLInterval: integer;
  sHTMLDirection: string;
  sError: string;


  procedure dll_GetSerialize(out strOut: pChar); external 'RevenueExpress.dll';
  procedure dll_GenMD5(const strHash: pChar; Out strOut: pChar);  external 'RevenueExpress.dll';
  procedure dll_GetRegistry(const strPName: PChar; const strKey: PChar;Out strOut: PChar); external 'RevenueExpress.dll';
  function dll_AddRegistry(const strPName: PChar;const strKey: pChar; const strValue: pChar): boolean; external 'RevenueExpress.dll';
  procedure FreePChar(p: Pchar); external 'RevenueExpress.dll';

implementation

{$R *.dfm}
uses
  fmLog, data;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  // hide application from task bar
  TStart.Enabled:=false;
  ShowWindow(Application.Handle, SW_HIDE);
  SetWindowLong(Application.Handle, GWL_EXSTYLE,
          GetWindowLong(Application.Handle, GWL_EXSTYLE) or WS_EX_TOOLWINDOW);

//  KeyHook.AddHotKey(vk_f8);
//  KeyHook.AddHotKey(vk_f9);
  KeyHook.AddHotKey(vk_f10);
  CompactDB;
  TStart.Enabled:=true;

  sProduct:='InfoExpress-Lite';
  sRegName01:='ComputerID';

  dll_AddRegistry(cProduct,cKeyAgent,cValAgent);
  dll_getRegistry(cProduct,pchar(sRegName01),pData01);
  sTmp01:=pData01;
  FreePChar(pData01);
  if (length(sTmp01) > 0) then
    begin
    sComputerID:=sTmp01;
    end
  Else
    begin
    dll_getSerialize(pData01);
    sTmp01:=pData01;
    FreePChar(pData01);

    dll_genMD5(pchar(sTmp01),pData01);
    sComputerID:=pData01;
    FreePChar(pData01);
    dll_addRegistry(pchar(sProduct),pchar(sRegName01),pchar(sComputerID));
    end;

  txtSerial.Text:=sComputerID;
  dll_getRegistry(cProduct,pchar(cKeyMonitor),pData01);
  sValMonitor:=pData01;
  FreePChar(pData01);

  dll_getRegistry(cProduct,pchar(cKeyPlayer),pData01);
  sValPlayer:=pData01;
  FreePChar(pData01);

  sValAgent:=cValAgent;
  ReadConfig;
end;

procedure TfrmMain.TStartTimer(Sender: TObject);
begin
  try
    if not InProcess then begin
        frmLog.DownloadConfig();
        frmLog.SendPlayLog();
//      frmLog.btnConnect.Click;
      if not bDownloadProcess then begin
//        frmLog.btnDownRefresh.Click;
        frmLog.DownloadFiles();
      end;
    end;
  finally
    TStart.Enabled := False;
// Insert By Boripat 05/09/2011
//
//    InProcess:=False;
//    Timer1.Enabled:=True;
// End Insert
  end;
end;

procedure TfrmMain.Timer1Timer(Sender: TObject);
begin
  ReadConfig;
  if not InProcess then begin
//    frmLog.btnConnect.Click;
    frmLog.DownloadConfig();
    frmLog.SendPlayLog();
    if not bDownloadProcess then begin
//      frmLog.btnDownRefresh.Click;
        frmLog.DownloadFiles();
    end;
  end;
{  sleep(1000);

  if not bDownloadProcess then begin
    frmLog.btnDownRefresh.Click;
  end;
}
end;

procedure TfrmMain.ShowLog1Click(Sender: TObject);
begin
    if (frmLog.Showing) then begin
      frmLog.Hide;
    end else begin
      frmLog.Show;
      frmLog.SetFocus;
    end;
{
  if not frmLog.Showing then
  begin
    frmLog.Show;
    frmLog.SetFocus;
  end
  else
  begin

  end;

  frmLog.BringToFront;

  SetWindowPos(Application.Handle,
               HWND_TOPMOST,
               0, 0, 0, 0, SWP_NOSIZE);

}
end;

procedure TfrmMain.HideLog1Click(Sender: TObject);
begin
  if frmLog.Showing then frmLog.Hide;
end;

procedure TfrmMain.Exit1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.RxTrayIcon1DblClick(Sender: TObject);
begin
  ShowLog1.Click;
end;

procedure TfrmMain.KeyHookHotKeyPressed(HotKey: Cardinal;
  Index: Word);
begin
//  if HotKey = vk_f8 then ShowLog1.Click;
//  if HotKey = vk_f9 then HideLog1.Click;
  if HotKey = vk_f10 then begin
    if (frmLog.Showing) then begin
      frmLog.Hide;
    end else begin
      frmLog.Show;
      frmLog.SetFocus;
    end;
  end;
end;

function TfrmMain.CompactAndRepair: Boolean; {DB = Path to Access Database}
var
  v: OLEvariant;
begin
  Result := True;
  try
    if (FileExists('dbx.mdb')) then  DeleteFile('dbx.mdb');
    v := CreateOLEObject('JRO.JetEngine');
    try
      begin
      V.CompactDatabase('Provider=Microsoft.Jet.OLEDB.4.0;Jet OLEDB:Database Password=9999;Data Source=db.mdb',
                        'Provider=Microsoft.Jet.OLEDB.4.0;Jet OLEDB:Database Password=9999;Data Source=dbx.mdb');

      DeleteFile('db.mdb');
      RenameFile('dbx.mdb', 'db.mdb');
      if (FileExists('dbx.mdb')) then  DeleteFile('dbx.mdb');
      end;
    except
      if (FileExists('dbx.mdb')) then  DeleteFile('dbx.mdb');
      Result:=false;
    end;
  except
    if (FileExists('dbx.mdb')) then  DeleteFile('dbx.mdb');
    Result := False;
  end;
end;

procedure TfrmMain.CompactDB;
begin
  If dm.db.Connected then dm.db.Close;

  try
    if CompactAndRepair then
    begin
//      MessageDlg('Database has been compact successfully.', mtInformation, [mbOK], 0);
    end
    else
    begin
      MessageDlg('Database compacting error! Close DS and DS Agent before start compact.', mtInformation, [mbOK], 0);
      Application.Terminate;
//      Self.Close;
    end;
  finally
  end;

end;

procedure TfrmMain.ReadConfig();
var
  iniFile: TIniFile;
begin
  iniFile:=TiniFile.Create(sProgramDir+'display.ini');
  try
    iPlayerLeft:=iniFile.ReadInteger('Placement','left',0);
    iPlayerTop:=iniFile.ReadInteger('Placement','top',0);
    iPlayerWidth:=iniFile.ReadInteger('Placement','width',640);
    iPlayerHeight:=iniFile.ReadInteger('Placement','height',480);
    bFullScreen:=(1=iniFile.ReadInteger('Placement','fullscreen',1));
    bPlayerRatio:=(1=iniFile.ReadInteger('video','ratio',1));
  finally
    iniFile.Free;
  end;
end;

procedure TfrmMain.CreateURLConfig();
var
  iniFile: TIniFile;
  iRun:integer;
  sName,sURL:string;
begin
  iniFile:=TiniFile.Create(sProgramDir+'url.ini');
  try
    if (bHTMLShow) then begin
      iniFile.WriteInteger('main','active',1);
    end else begin
      iniFile.WriteInteger('main','active',0);
    end;
    iniFile.WriteInteger('main','interval',iHTMLInterval);
    iniFile.WriteInteger('main','items',iHTMLItem);
    iniFile.WriteString('main','direction',sHTMLDirection);
    iniFile.WriteInteger('main','size',iHTMLSize);
    for iRun:=1 to iHTMLItem do begin
      sName:='url'+inttostr(iRun);
      sURL:=arrURL[iRun];
      iniFile.WriteString('url',sName,sURL);
    end;
  except
    on E : Exception do begin
      sError:='Exception on Function = CreateURLConfig';
      sError:=sError+#13#10+'Exception Class = '+E.ClassName;
      sError:=sError+#13#10+'Exception Message = '+E.Message;
      ShowMessage(sError);
    end;
  end;
  iniFile.Free;
end;

end.
