unit fmLog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AdvOfficePager, Grids, DBGrids, SMDBGrid, StdCtrls, Buttons, DB,
  ComCtrls, GtroDBDateTimePicker, AdvGlassButton, DBCtrls, Mask, RXCtrls,
  ExtCtrls, jpeg, IdBaseComponent, IdComponent, IdTCPConnection, ADODB,
  IdTCPClient, IdHTTP, FileCtrl, DateUtils, RXShell, Menus, NB30, Math,
  IdAntiFreezeBase, IdAntiFreeze, RXClock,strUtils, INIFILES,IdHashMessageDigest, idHash,
  AdvProgr;

type
  TfrmLog = class(TForm)
    AdminPage: TAdvOfficePager;
    tbConfig: TAdvOfficePage;
    Image3: TImage;
    RxLabel11: TRxLabel;
    IdHTTP1: TIdHTTP;
    Memo3: TMemo;
    RxLabel1: TRxLabel;
    DBEdit1: TDBEdit;
    RxLabel2: TRxLabel;
    DBEdit2: TDBEdit;
    RxLabel16: TRxLabel;
    DBEdit9: TDBEdit;
    IdAntiFreeze1: TIdAntiFreeze;
    ProgressBar1: TProgressBar;
    btnHide: TSpeedButton;
    RxLabel4: TRxLabel;
    DBEdit3: TDBEdit;
    Timer1: TTimer;
    RxClock1: TRxClock;
    tbDebug: TAdvOfficePage;
    Memo2: TMemo;
    Memo1: TMemo;
    RxLabel6: TRxLabel;
    timRecord: TTimer;
    txtCode: TEdit;
    btnRefresh: TButton;
    btnMinimize: TButton;
    rxlblVersion: TRxLabel;
    edtMac: TEdit;
    btnConnect: TButton;
    IdHTTPDown1: TIdHTTP;
    timChkDown: TTimer;
    GroupBox1: TGroupBox;
    mmoDown: TMemo;
    lblDown1: TRxLabel;
    btnDownRefresh: TButton;
    IdHTTPLog: TIdHTTP;
    barDown2: TProgressBar;
    lblDown2: TRxLabel;
    btnDownStop: TButton;
    lstDownload: TListBox;
    timDownload: TTimer;
    barDown1: TAdvProgress;
    dbtxtQID: TDBEdit;
    mmoDebug: TMemo;
    btnDebugClear: TButton;
    procedure btnHTTPClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure IdHTTP1Work(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCount: Integer);
    procedure IdHTTP1WorkBegin(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCountMax: Integer);
    procedure IdHTTP1WorkEnd(Sender: TObject; AWorkMode: TWorkMode);
    procedure btnHideClick(Sender: TObject);
    procedure Hide1Click(Sender: TObject);
    procedure Image3DblClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure gbtnMinimizeClick(Sender: TObject);
    procedure timRecordTimer(Sender: TObject);
    procedure btnConnectClick(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
    procedure btnDownRefreshClick(Sender: TObject);
    procedure IdHTTPDown1Work(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCount: Integer);
    procedure IdHTTPDown1WorkBegin(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCountMax: Integer);
    procedure IdHTTPDown1WorkEnd(Sender: TObject; AWorkMode: TWorkMode);
    procedure IdHTTPLogWorkBegin(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCountMax: Integer);
    procedure IdHTTPLogWork(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCount: Integer);
    procedure IdHTTPLogWorkEnd(Sender: TObject; AWorkMode: TWorkMode);
    procedure btnDownStopClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure timChkDownTimer(Sender: TObject);
    procedure btnDebugClearClick(Sender: TObject);
  private
    { Private declarations }
    procedure DBconnect;

    procedure ShowLog(input: string;memo: TMemo);
    procedure ClearLog;

    procedure GetServerQ;
    procedure GetServerActiveQF;
    procedure GetServerNews;
    procedure GetServerBanner;

    procedure GetAllVideos;
    procedure GetVideoFiles;
    procedure GetVideoFile(forg,fname, floc: string);
//    procedure GetVideoFile(fname, floc: string);

    procedure GetBannerFiles;
    procedure GetBannerFile(fname, floc: string);

  public
    { Public declarations }
    function GenHTTPRequest(forvideo: boolean; cmd: integer; param1: string): string;
    function ExtractResponse(Input: string): TStringList;
    function GenDateTime(input: string): TDateTime;
    procedure ManageQ;
    procedure ManageQB1;
    procedure ManageQB2;
    function ZeroPad(input: string; cnt: integer): string;
    function ReplaceSpace(input: string): string;
    function DateTimeForSQL(const dateTime : TDateTime): string;

    function GetFileSize(fileName : wideString) : Int64;
//    function GetFileSize(fname: string): integer;

    function GetAdapterInfo(Lana: Char): String;
    function GetMACAddress: String;

    procedure GetClientConfig;
    procedure PutClientConfig;
    procedure SaveLog(sCategory: string; sValue:string);
    procedure TrackLog(sType,sValue:string);
    procedure PutClientOnline(sType:string; sCategory:string);
    procedure PutClientData(sKey,sValue: string);
    procedure UpdateConfigVariable;
    procedure TranslateDOW(input: string);
    function GenTimeInterval(input: string): TDateTime;
    function FileTimeToDTime(FTime: TFileTime): TDateTime;
    function checkModify(sType:string; sRecord: string):string;
    procedure DownloadFile(forg,fname,floc,fhash:string;idDown:TidHTTP);
    function GetFileDate(sFileName,sType: string):string;
    function CheckFileComplete(sFileName,sOrgName:string): boolean;
    procedure AddFileComplete(sName,sFileName:string);
    procedure DownloadConfig();
    procedure DownloadFiles();
    procedure DownloadStop();
    procedure SendPlayLog();
    procedure GetAllData();
    procedure GetHTMLConfig();
    procedure GetHTMLURL();
    procedure GetNewClientConfig();
  end;

var
  frmLog: TfrmLog;
  Output: TStringList;
  MemoryStream: TMemoryStream;
  MemoryDownload: TMemoryStream;
  ResponseStr: TStringList;

  bTrack: boolean;
  iClick: integer;
  Client_ID: integer;
  Terminal_ID: integer;
  Client_PWD: string;
  MACAddr: string;

  Show_News: Boolean;
  Show_Ads: Boolean;
  Video_Dir: string;
  Def_video: string;
  http_server: string;
  video_program: string;
  news_program: string;
  refresh_time: integer;
  ftp_server: string;
  ftp_port: integer;
  ftp_user: string;
  ftp_pass: string;
  ftp_path: string;
  ftp_passive: boolean;
  debug_mode: boolean;
  sSMPLog: string;
  sAGNLog: string;
  sMONLog: string;
  sTrackLog: string;
  iInterval: integer;
  iOfflineInterval: integer;
  FullVideoDir: string;

  InProcess: boolean;
  BannerMode: integer;
  sProgramDir: string;
  bDownloadProcess: boolean;
  bDownTrack1:boolean;
  bDownTrack2:boolean;
  iLastDownloadPos: integer;
  iDownloadCounter: integer;
  iLoopDownload:integer;

  sHTTP:string;  // Keep HTTP Command;
  sError:string; // Keep Exception String;

implementation

uses
  fmMain, data;

{$R *.dfm}

function IsConnected : BOOLEAN;
type
  TGetConnectedState = function(var Flags : Integer; Reserved : Integer) :Bool; StdCall;
var
  WlLib : THandle;
  GetCS : TGetConnectedState;
  State : Integer;
begin
  result:=false;
  WlLib := LoadLibrary('wininet.dll');
  if WlLib = 0 then begin
    Exit;
  end;
  GetCS := GetProcAddress(WlLib, 'InternetGetConnectedState');
  if @GetCS <> nil then begin
    if GetCS(State, 0) then begin
// Connection detected, place your code or a
// procedure / function call here... for instance:
      FreeLibrary(WlLib);
      result:=true;
    end else begin
// No Connection, do your thing...
      FreeLibrary(WlLib);
      result:=false;
    end;
  end;
end;

function getMD5checksum(const fileName : string) : string;
 var
   idmd5 : TIdHashMessageDigest5;
   fs : TFileStream;
   hash : T4x4LongWordRecord;
 begin
   idmd5 := TIdHashMessageDigest5.Create;
   fs := TFileStream.Create(fileName, fmOpenRead OR fmShareDenyWrite) ;
   try
     result := idmd5.AsHex(idmd5.HashValue(fs)) ;
   finally
     fs.Free;
     idmd5.Free;
   end;
 end;

procedure Split(const Delimiter: Char;Input: string;
    const Strings: TStrings) ;
begin
   Assert(Assigned(Strings)) ;
   Strings.Clear;
   Strings.Delimiter := Delimiter;
   Strings.DelimitedText := Input;
end;

function FileInUse(FileName: string): Boolean;
var hFileRes: HFILE;
begin
  Result := False;
  if not FileExists(FileName) then exit;
  hFileRes := CreateFile(PChar(FileName),
                                    GENERIC_READ or GENERIC_WRITE,
                                    0,
                                    nil,
                                    OPEN_EXISTING,
                                    FILE_ATTRIBUTE_NORMAL,
                                    0);
  Result := (hFileRes = INVALID_HANDLE_VALUE);
  if not Result then
    CloseHandle(hFileRes);
end;

function TfrmLog.GetAdapterInfo(Lana: Char): String;
var
  Adapter: TAdapterStatus;
  NCB: TNCB;
begin
  FillChar(NCB, SizeOf(NCB), 0);
  NCB.ncb_command := Char(NCBRESET);
  NCB.ncb_lana_num := Lana;
  if Netbios(@NCB) <> Char(NRC_GOODRET) then
  begin
    Result := 'No MAC';
    Exit;
  end;

  FillChar(NCB, SizeOf(NCB), 0);
  NCB.ncb_command := Char(NCBASTAT);
  NCB.ncb_lana_num := Lana;
  NCB.ncb_callname := '*';

  FillChar(Adapter, SizeOf(Adapter), 0);
  NCB.ncb_buffer := @Adapter;
  NCB.ncb_length := SizeOf(Adapter);

  if Netbios(@NCB) <> Char(NRC_GOODRET) then
  begin
    Result := 'No MAC';
    Exit;
  end;

  Result :=
    IntToHex(Byte(Adapter.adapter_address[0]), 2) + ':' +
    IntToHex(Byte(Adapter.adapter_address[1]), 2) + ':' +
    IntToHex(Byte(Adapter.adapter_address[2]), 2) + ':' +
    IntToHex(Byte(Adapter.adapter_address[3]), 2) + ':' +
    IntToHex(Byte(Adapter.adapter_address[4]), 2) + ':' +
    IntToHex(Byte(Adapter.adapter_address[5]), 2);
end;

function TfrmLog.GetMACAddress: string;
var
  AdapterList: TLanaEnum;
  NCB: TNCB;
begin
  FillChar(NCB, SizeOf(NCB), 0);
  NCB.ncb_command := Char(NCBENUM);
  NCB.ncb_buffer := @AdapterList;
  NCB.ncb_length := SizeOf(AdapterList);
  Netbios(@NCB);

  if Byte(AdapterList.length) > 0 then
    Result := GetAdapterInfo(AdapterList.lana[0])
  else
    Result := 'No MAC';
end;

procedure TfrmLog.DBconnect;
begin
  with dm.db do
  begin
    if Connected then Connected := False;
    ConnectionString := 'Provider=Microsoft.Jet.OLEDB.4.0;Jet OLEDB:Database Password=9999;Data Source=db.mdb;Persist Security Info=True';
    Connected := True;
  end;

  with dm do
  begin
    tblConfig.Open;
    tblPlayLayout.Open;
    tblHTMLURl.Open;
  end;

  UpdateConfigVariable;

  if refresh_time > 0 then
    frmMain.Timer1.Interval := refresh_time * 60 * 1000
//    frmMain.Timer1.Interval := refresh_time * 1000
  else
    frmMain.Timer1.Interval := 1200000;          // default = 20 minutes.

  frmMain.Enabled := True;
end;

//--------------------------------------------------------------------------------
//  Start and Ending Event of Admin Page
//--------------------------------------------------------------------------------
procedure TfrmLog.FormCreate(Sender: TObject);
var iniFile: TINIFile;
begin
  rxlblVersion.Caption:='Version '+cValAgent;
  DBconnect;
  txtCode.Text:=AnsiLeftStr(frmMain.txtSerial.Text,4)
    + '-'+ AnsiRightStr(frmMain.txtSerial.Text,4);
  sProgramDir:=ExtractFilePath(Application.ExeName);
  FullVideoDir := ExtractFilePath(Application.ExeName) + Video_Dir;

  if FileExists( FullVideoDir + 'logo.jpg' ) then
  begin
    image3.Picture.LoadFromFile(FullVideoDir + 'logo.jpg');
  end;

  Output := TStringList.Create;
  MemoryStream := TMemoryStream.Create;
  MemoryDownload := TMemoryStream.Create;
  ResponseStr := TStringList.Create;

  InProcess := False;
  bDownloadProcess := False;
  bDownTrack1:=false;
  bDownTrack2:=false;

  MACAddr := GetMACAddress;
  edtMAC.Text := MACAddr;
  bTrack:=false;
  sAGNLog:='online_agn';
  sTrackLog:='track_agn';
  iOfflineInterval:=180;
  iniFile:=TiniFile.Create(sProgramDir+'config.ini');
  try
    sSMPLog:=iniFile.ReadString('online_log','player','online_smp');
    sAGNLog:=iniFile.ReadString('online_log','agent','online_agn');
    sMONLog:=iniFile.ReadString('online_log','monitor','online_mon');
    bTrack:=(1=iniFile.ReadInteger('track','agent',1));
    sTrackLog:=iniFile.ReadString('track_log','agent','track_agn');
    iInterval:=iniFile.ReadInteger('online_status','interval',1);
    iOfflineInterval:=iniFile.ReadInteger('online_status','offline',10);
  finally
    iniFile.Free;
  end;
  SaveLog(sAGNLog,formatdatetime('yyyy-mm-dd hh:nn:ss',now));
  iLoopDownload:=0;
  TrackLog('debug','Program Start');
  TrackLog('Download','Program Start');
  timRecord.Interval:=1000*iInterval;
  timRecord.Enabled:=true;
  frmLog.Caption:='DS Agent '+cValAgent;
  frmLog.Height:=AdminPage.Top+AdminPage.Height+25;
end;

procedure TfrmLog.FormDestroy(Sender: TObject);
begin
  //ResponseStr.Free;
  TrackLog('debug','Program Close');
  Output.Free;
  MemoryStream.Free;
end;

procedure TfrmLog.ShowLog(input: string; memo:TMemo);
begin
//  Memo3.Lines.Add(FormatDateTime('dd/mm/yyyy hh:nn:ss', now) + ' - ' + input);
  memo.Lines.Add(FormatDateTime('dd/mm/yyyy hh:nn:ss', now) + ' - ' + input);
end;

procedure TfrmLog.ClearLog;
begin

end;

//--------------------------------------------------------------------------------
// Main function for manager data with server
//--------------------------------------------------------------------------------
procedure TfrmLog.btnHTTPClick(Sender: TObject);
begin
// Insert By Boripat 05/09/2011
//  InProcess := True;
  if (not InProcess) then
  begin
// End Insert
  InProcess := True;
  TrackLog('Config','Start Connection');
  try
    frmLog.Memo3.Clear;
    frmMain.Timer1.Enabled:=false;
    TrackLog('Config','GetClientConfig');
    GetClientConfig;
    TrackLog('Config','UpdateConfigVariable');
    UpdateConfigVariable;
    Application.ProcessMessages;

    TrackLog('Config','GetServerQ');
    GetServerQ;
    Application.ProcessMessages;

    if Show_News then
      begin
      TrackLog('Config','GetServerNews');
      GetServerNews;
      end;
    Application.ProcessMessages;

    if Show_Ads then
    begin
      TrackLog('Config','GetServerBanner');
      GetServerBanner;
      Application.ProcessMessages;

      if BannerMode = 1 then
      begin
        TrackLog('Config','ManageQB1');
        ManageQB1;
        Application.ProcessMessages;
      end
      else
      begin
        TrackLog('Config','ManageQB1');
        ManageQB1;
        Application.ProcessMessages;

        TrackLog('Config','ManageQB2');
        ManageQB2;
        Application.ProcessMessages;
      end
    end;

    TrackLog('Config','GetBannerFiles');
    GetBannerFiles;
    Application.ProcessMessages;

    TrackLog('Config','GetServerActiveQF');
    GetServerActiveQF;
    Application.ProcessMessages;

    TrackLog('Config','ManageQ');
    ManageQ;
    Application.ProcessMessages;

//    GetAllVideoes;
    TrackLog('Config','GetVideoFiles');
    GetVideoFiles;
    Application.ProcessMessages;

    GetAllVideos;
    Application.ProcessMessages;

    if dm.tblConfig.State = dsedit then
      dm.tblConfig.Post;

    TrackLog('Config','putClientOnline');

    putClientOnline('Player',sSMPLog);
    Application.ProcessMessages;

    putClientOnline('Agent',sAgnLog);
    Application.ProcessMessages;

    putClientOnline('Monitor',sMonLog);
    Application.ProcessMessages;

  finally
    InProcess := False;
    frmMain.Timer1.Interval:=1000*60*refresh_time;
    frmMain.Timer1.Enabled:=true;
  end;
// Insert By Boripat 05/09/2011
  end;
// End Insert
end;

procedure TfrmLog.GetServerQ;
var
  i: integer;
//  sHTTP:string;
begin
  // get all queue record in server
  if (Not IsConnected) then begin
    ShowLog('Error: No Intenet Connection',memo3);
    exit;
  end;
  try
    Application.ProcessMessages;
    MemoryStream.Clear;
    sHTTP:=GenHTTPRequest(true, 0, inttostr(client_id));
    mmoDebug.Lines.Add(sHTTP);
    IdHTTP1.get(sHTTP, MemoryStream);
    Application.ProcessMessages;
    MemoryStream.Position := 0;
    Memo2.Clear;
    Memo2.Lines.LoadFromStream(MemoryStream);
    mmoDebug.Lines.Add(Memo2.Lines.Text);
    ShowLog('Finish: Get Queue List from Server Client_id = ' + inttostr(client_id),Memo3);
  except
    ShowLog('Error: Cannot get Queue List from Server Client_id = ' + inttostr(client_id),Memo3);
    abort;
  end;

    // add all queue from server to local database
  with dm.tblQ do
  begin
    dm.qryClearQ.ExecSQL;
    if Active then Close;
    Open;

    for i := 1 to Memo2.Lines.Count - 1 do
    begin
      try
        ResponseStr := ExtractResponse(Memo2.Lines[i]);

        append;
        FieldByName('id').AsInteger := strtoint(ResponseStr.Strings[0]);
        FieldByName('queue_no').AsString := ResponseStr.Strings[2];
        FieldByName('clientid').AsInteger := client_id;
        FieldByName('terminalid').AsInteger := terminal_id;

        FieldByName('qname').AsString := ResponseStr.Strings[3];
        FieldByName('startdate').AsDateTime := GenDateTime(ResponseStr.Strings[4]);
        FieldByName('stopdate').AsDateTime := GenDateTime(ResponseStr.Strings[5]);

        FieldByName('active').AsBoolean := ResponseStr.Strings[6] = 'Y';
        FieldByName('running').AsBoolean := ResponseStr.Strings[7] = 'Y';

//  Insert by Boripat 05/09/2011
        TranslateDOW(ResponseStr.Strings[8]);
        FieldByName('startinterval').AsDateTime := GenTimeInterval(ResponseStr.Strings[9]);
        FieldByName('endinterval').AsDateTime := GenTimeInterval(ResponseStr.Strings[10]);
        FieldByName('order').AsInteger := strtoint(ResponseStr.Strings[11]);

//  End Insert
        post;
        Application.ProcessMessages;
      except
        ShowLog('Error: Write Queue List to local DB queue no = ' + ResponseStr.Strings[2],Memo3);
      end;
    end;
  end;
end;

procedure TfrmLog.GetServerActiveQF;
var
  i: integer;
  bFound: boolean;
  qno: string;
  id: integer;
  req: string;
begin

  if (Not IsConnected) then begin
    ShowLog('Error: No Intenet Connection',memo3);
    exit;
  end;

  with dm.qryGetActiveQ do
  begin
    if active then close;
    open;

    if not Fields[0].IsNull then
      qno := Fields[1].AsString
    else
      qno := '';
  end;

  try
    MemoryStream.Clear;

    if qno <> '' then
    begin
      // get only files of running Queue from server
      Application.ProcessMessages;
      sHTTP:=GenHTTPRequest(true, 1, qno);
//      IdHTTP1.get(GenHTTPRequest(true, 1, qno), MemoryStream);
      IdHTTP1.get(sHTTP, MemoryStream);
      Application.ProcessMessages;
      ShowLog('Finish: Get Active File List from Server Queue No. = ' + qno,Memo3);

      // **** important part locate queue
      dm.tblQ.Locate('queue_no', qno, []);
//      showLog('Request'+req,mmoDown);
    end
    else
    begin
      ShowLog('No active queue in queue list. (Check Queue Active, Start Date and Stop Date)',memo3);
//
      Application.ProcessMessages;
      sHTTP:=GenHTTPRequest(true, 1, '');
//      IdHTTP1.get(GenHTTPRequest(true, 1, ''), MemoryStream);
      IdHTTP1.get(sHTTP, MemoryStream);
      Application.ProcessMessages;
//      ShowLog('Finish: Get Active File List from Server Queue No. = ' + qno);
//
    end;

    MemoryStream.Position := 0;
    Memo2.Clear;
    Memo2.Lines.LoadFromStream(MemoryStream);

  except
    ShowLog('Error: Cannot get Active File List from Server Queue No. = ' + qno,memo3);
    abort;
  end;

  with dm.tblQF do
  begin
    dm.qryclearQF.ExecSQL;
    if Active then Close;
    Open;

    for i := 1 to Memo2.Lines.Count - 1 do
    begin

      try
        ResponseStr := ExtractResponse(Memo2.Lines[i]);
//        dm.tblQF.First;
//        id:=strtoint(ResponseStr.Strings[0]);
//        bFound:=dm.tblQF.Locate('id',id,[]);
//        if (bFound) then
//          begin
//          dm.tblQF.Edit;
//          end
//        else
//          begin
//          end;
        append;
        FieldByName('id').AsInteger := strtoint(ResponseStr.Strings[0]);
//        FieldByName('qid').AsInteger := dm.tblQID.AsInteger; // field to links Q and Q files
        FieldByName('qid').AsInteger := strtoint(ResponseStr.Strings[1]); // field to links Q and Q files
        FieldByName('clientid').AsInteger := client_id;
        FieldByName('terminalid').AsInteger := terminal_id;

        FieldByName('filename').AsString := ResponseStr.Strings[2];
        FieldByName('newfilename').AsString := ResponseStr.Strings[8];

        FieldByName('filetype').AsString := ResponseStr.Strings[3];
        FieldByName('filesize').AsInteger := strtoint(ResponseStr.Strings[4]);
        FieldByName('filelocation').AsString := ResponseStr.Strings[5];
        FieldByName('filehash').AsString := ResponseStr.Strings[10];
        FieldByName('active').AsBoolean := ResponseStr.Strings[6] = 'Y';
        FieldByName('avail').AsBoolean := True;

        if ResponseStr.Strings[7] <> '' then
          FieldByName('ordering').AsInteger := strtoint(ResponseStr.Strings[7])
        else
          FieldByName('ordering').AsInteger := 0;

        post;
        ShowLog('Finish: Write File List to local DB File Name = ' + ResponseStr.Strings[2],memo3);
        Application.ProcessMessages;
      except
        ShowLog('Error: Cannot Write File List to local DB File Name = ' + ResponseStr.Strings[2],memo3);
      end;
    end;
  end;
end;

procedure TfrmLog.ManageQ;
begin
  dm.qryClearPlay.ExecSQL;
  dm.RefreshPlayQ;

  with dm.qryGetActiveFile do
  begin
    if active then close;
    open;

    ShowLog('Update Playing Queue - ' + IntToStr(RecordCount) + ' Videos',memo3);

    first;
    while not eof do
    begin
      dm.tblPlayQ.Append;
      dm.tblPlayQID.AsInteger := fieldbyname('id').AsInteger;
      dm.tblPlayQQID.AsInteger := fieldbyname('qid').AsInteger;
      dm.tblPlayQ.Post;
      next;
    end;
  end;
end;


procedure TFrmLog.GetAllVideos;
var sName,sFile,sPath,sHash,sLog:string;
  bDownloadFile: boolean;
begin
  if (dm.tblFiles.Active) then dm.tblFiles.Close;
  dm.tblFiles.Open;

  dm.tblFiles.First;
  while (not dm.tblFiles.Eof) do begin
//    if (dm.tblFiles.FieldByName('filetype').AsString <> 'URL') and
//      dm.tblFiles.FieldByName('active').AsBoolean then
    inc(iLoopDownload);
    if (dm.tblFiles.FieldByName('filetype').AsString = 'URL') then begin
// Streaming Server
      sLog:=inttostr(iLoopDownload)+'Skip Streaming URL '+ dm.tblFiles.FieldByName('filelocation').AsString;
      TrackLog('Download',sLog);
    end else begin
      bDownloadFile:=true;
      sName:=dm.tblFiles.FieldByName('filename').AsString;
      sFile:=dm.tblFiles.FieldByName('newfilename').AsString;
      sPath:=dm.tblFiles.FieldByName('filelocation').AsString;
      sHash:=dm.tblFiles.FieldByName('fileHash').AsString;

      bDownloadFile:=not checkFileComplete(sFile,sName);

{
      if (FileExists(FullVideoDir + sFile)) then begin
        if (sHash = getMD5CheckSum(FullVideoDir + sFile)) then begin
          bDownloadFile:=false;
        end;
      end;
}
//      ShowLog('hash =>'+sHash+' = '+ getMD5CheckSum(FullVideoDir + sFile),mmoDown);

      if ( bDownloadFile) then begin
        try
          sLog:='Download File '+sName;
//          ' Size: '+Format('%n KB',[dm.tblFiles.FieldByName('filesize').AsFloat/1024]);
          ShowLog(sLog,mmoDown);
//          lblDown1.Caption:=sLog;
          DownloadFile(sName,sFile,sPath,sHash,idHTTPDown1);
//          GetVideoFile(sName,sFile, sPath);
          Application.ProcessMessages;
        except
          if IdHTTP1.Connected then IdHTTP1.Disconnect;
          Application.ProcessMessages;
        end;
      end else begin
        ShowLog('File '+sName+' Exists. ',mmoDown);
      end;
    end;
    dm.tblFiles.Next;
  end;
end;

procedure TfrmLog.GetVideoFiles;
var sName,sFile,sPath,sHash,sLog:string;
  bDownload:boolean;
begin
  dm.RefreshPlayQ;

  dm.tblPlayQ.First;
  while not dm.tblPlayQ.Eof do
  begin

    with dm.tblQF do
    begin
      if Locate('id', dm.tblPlayQID.AsInteger, []) then
      begin
        inc(iLoopDownload);
        if (FieldByName('filetype').AsString = 'URL') then begin
          sLog:=inttostr(iLoopDownload)+'Skip Streaming URL '+ FieldByName('filelocation').AsString;
          TrackLog('Download',sLog);
        end else begin
// Mark By Boripat 05/09/2011
//          if not FileExists(FullVideoDir + FieldByName('newfilename').AsString) then
//          begin
//            // if file is not in client, download it.
//            GetVideoFile(FieldByName('newfilename').AsString, FieldByName('filelocation').AsString);
//            Application.ProcessMessages;
//          end
//          else
//          begin
//            edit;
//            FieldByName('avail').AsBoolean := True;
//            post;
//          end;
// End Mark
//  Insert By Boripat 05/09/2011
          sName:=FieldByName('filename').AsString;
          sFile:=FieldByName('newfilename').AsString;
          sPath:=FieldByName('filelocation').AsString;
          sHash:=FieldByName('fileHash').AsString;

          bDownload:=not checkFileComplete(sFile,sName);
{
          if (FileExists(FullVideoDir + sFile)) then begin
            if (sHash = getMD5CheckSum(FullVideoDir + sFile)) then begin
              bDownload:=false;
            end;
          end;
}
//          ShowLog('hash =>'+sHash+' = '+ getMD5CheckSum(FullVideoDir + sFile),mmoDown);

          if ( bDownload) then
          begin
            try
//              GetVideoFile(sName,sFile,sPath);
              sLog:='Start Download File '+sName;
//              +' Size: '+
//                Format('%n KB',[FieldByName('filesize').AsFloat/1024]);
              ShowLog(sLog,mmoDown);
//              lblDown1.Caption:=sLog;
              DownloadFile(sName,sFile,sPath,sHash,idHTTPDown1);
            except
              if IdHTTPDown1.Connected then IdHTTPDown1.Disconnect;
              Application.ProcessMessages;
            end;
            Application.ProcessMessages;
          end else begin
            ShowLog('File '+sName+' Exists. ',mmoDown);
          end;
//  End Insert
        end;
      end;
    end;

    dm.tblPlayQ.Next;
  end;
end;

procedure TfrmLog.DownloadFile(forg,fname,floc,fhash:string;idDown:TidHTTP);
var sRequest,sHash: string;
begin

  if (Not IsConnected) then begin
    ShowLog('Error: No Intenet Connection',mmoDown);
    exit;
  end;
  try
    MemoryDownload.Clear;
    bDownloadProcess:=true;
    timChkDown.Enabled:=true;

//    ShowLog('Start: Get File ' + forg,mmoDown);
//    ShowLog('Start: Get Video File ' + dm.tblQF.FieldByName('filename').AsString);
//    ShowLog('Start: Get Video File ' + dm.tblFiles.FieldByName('filename').AsString);
    sRequest:=floc+'?mc='+frmMain.txtSerial.Text;

    TrackLog('Download',inttostr(iLoopDownload)+' Start Download => "'+forg+'" =>"'+fname+'" ');
//    IdHTTP1.get(floc, MemoryStream);
    idDown.get(sRequest, MemoryDownload);
//    Application.ProcessMessages;

    MemoryDownload.Position := 0;
    if (FileExists(FullVideoDir+fname)) then begin
      if (not DeleteFile(FullVideoDir+fname)) then begin
        TrackLog('Download',inttostr(iLoopDownload)+' Cannot Delete Local File => "'+forg+'" =>"'+fname+'" =>"'+sHash+'"');
        ShowLog('Error: File In Used ' + forg,mmoDown);
        MemoryDownload.Clear;
        bDownloadProcess:=false;
        timChkDown.Enabled:=false;
        exit;
      end;
    end;
    MemoryDownload.SaveToFile(FullVideoDir + fname);
//    Application.ProcessMessages;

//    ShowLog('Finish: Get Vide File ' + dm.tblQF.FieldByName('filename').AsString);
//    ShowLog('Finish: Get Video File ' + dm.tblFiles.FieldByName('filename').AsString);
    sHash:=getMD5CheckSum(FullVideoDir + fname);
    if (fHash = sHash) then begin
      TrackLog('Download',inttostr(iLoopDownload)+' Finish Download => "'+forg+'" =>"'+fname+'" =>"'+sHash+'"');
      ShowLog('Finish: Download File ' + forg,mmoDown);
      AddFileComplete(forg,fname);
    end else begin
      TrackLog('Download',inttostr(iLoopDownload)+' Error :: Hashing Fail "'+forg+'" =>"'+fname+'" hash=>"'+sHash+'" <> "'+fHash+'"');
      ShowLog('Error: Download File ' + forg,mmoDown);
    end;
//    ShowLog('file :' + forg+' => '+fname+' hash=>'+getMD5checksum(FullVideoDir + fname),mmoDown);

    timChkDown.Enabled:=false;
  except
    TrackLog('Download',inttostr(iLoopDownload)+' Error: Download Failed => "'+forg+'" =>"'+fname+'" ');
    ShowLog('Error: Cannot get file ' + forg,mmoDown);
//    ShowLog('Error: Cannot get video file ' + dm.tblQF.FieldByName('filename').AsString);
//    ShowLog('Error: Cannot get video file ' + dm.tblFiles.FieldByName('filename').AsString);
    timChkDown.Enabled:=false;
  end;
  MemoryDownload.Clear;
  bDownloadProcess:=false;
end;

procedure TfrmLog.GetVideoFile(forg,fname, floc: string);
var sRequest: string;
begin
  try
    Application.ProcessMessages;
    MemoryStream.Clear;

    ShowLog('Start: Get Video File ' + forg,mmoDown);
//    ShowLog('Start: Get Video File ' + dm.tblQF.FieldByName('filename').AsString);
//    ShowLog('Start: Get Video File ' + dm.tblFiles.FieldByName('filename').AsString);
    sRequest:=floc+'?mc='+frmMain.txtSerial.Text;

//    IdHTTP1.get(floc, MemoryStream);
    IdHTTPDown1.get(sRequest, MemoryStream);
    Application.ProcessMessages;

    MemoryStream.Position := 0;
    MemoryStream.SaveToFile(FullVideoDir + fname);
    Application.ProcessMessages;

//    ShowLog('Finish: Get Video File ' + dm.tblQF.FieldByName('filename').AsString);
//    ShowLog('Finish: Get Video File ' + dm.tblFiles.FieldByName('filename').AsString);
    ShowLog('Finish: Get Video File ' + forg,mmoDown);

  except
    ShowLog('Error: Cannot get video file ' + forg,mmoDown);
//    ShowLog('Error: Cannot get video file ' + dm.tblQF.FieldByName('filename').AsString);
//    ShowLog('Error: Cannot get video file ' + dm.tblFiles.FieldByName('filename').AsString);
  end;
end;

procedure TfrmLog.GetServerNews;
var
  i: integer;
//  sHTTP:string;
begin
  if (Not IsConnected) then begin
    ShowLog('Error: No Intenet Connection',memo3);
    exit;
  end;
  try
  // get all news record in server
    Application.ProcessMessages;
    MemoryStream.Clear;
    sHTTP:=GenHTTPRequest(false, 2, inttostr(client_id));
    mmoDebug.Lines.Add(sHTTP);
    IdHTTP1.get(sHTTP, MemoryStream);
    Application.ProcessMessages;
    MemoryStream.Position := 0;
    Memo2.Clear;
    Memo2.Lines.LoadFromStream(MemoryStream);
    mmoDebug.Lines.Add(Memo2.Lines.Text);
    ShowLog('Finish: Get News List from Server Client_id = ' + inttostr(client_id),memo3);
  except
    ShowLog('Error: Cannot get News List from Server Client_id = ' + inttostr(client_id),memo3);
    abort;
  end;

  // add all news from server to local database
  with dm.tblNews do
  begin

    dm.qryClearNews.ExecSQL;
    if Active then Close;
    Open;

    for i := 1 to Memo2.Lines.Count - 1 do
    begin
      try
        ResponseStr := ExtractResponse(Memo2.Lines[i]);
        append;
        FieldByName('id').AsInteger := strtoint(ResponseStr.Strings[0]);
        FieldByName('clientid').AsInteger := client_id;
        FieldByName('terminalid').AsInteger := terminal_id;

        FieldByName('qid').AsString := ResponseStr.Strings[1];
        FieldByName('headline').AsString := ResponseStr.Strings[3];
        FieldByName('startdate').AsDateTime := GenDateTime(ResponseStr.Strings[5]);
        FieldByName('stopdate').AsDateTime := GenDateTime(ResponseStr.Strings[6]);
        FieldByName('active').AsBoolean := ResponseStr.Strings[7] = 'Y';
        post;
        Application.ProcessMessages;
      except
        ShowLog('Error: Write News to local DB News_id = ' + ResponseStr.Strings[0],memo3);
      end;
    end;
  end;
end;

procedure TfrmLog.GetServerBanner;
var
  i: integer;
//  sHTTP:string;
begin
  if (Not IsConnected) then begin
    ShowLog('Error: No Intenet Connection',memo3);
    exit;
  end;
  try
  // get all Banner record in server
    Application.ProcessMessages;
    MemoryStream.Clear;
    sHTTP:=GenHTTPRequest(false, 12, inttostr(client_id));
    mmoDebug.Lines.Add(sHTTP);
    IdHTTP1.get(sHTTP, MemoryStream);
    Application.ProcessMessages;
    MemoryStream.Position := 0;
    Memo2.Clear;
    Memo2.Lines.LoadFromStream(MemoryStream);
    mmoDebug.Lines.Add(Memo2.Lines.Text);
    ShowLog('Finish: Get Banner List from Server id = ' + inttostr(client_id),memo3);
  except
    ShowLog('Error: Cannot get Banner List from Server id = ' + inttostr(client_id),memo3);
    abort;
  end;

  // add all Banner from server to local database
  with dm.tblBanner do
  begin

    dm.qryClearBanner.ExecSQL;
    if Active then Close;
    Open;

    for i := 1 to Memo2.Lines.Count - 1 do
    begin
      try
        ResponseStr := ExtractResponse(Memo2.Lines[i]);
//        if locate('id',strtoint(ResponseStr.Strings[0]),[]) then
//          continue;
        append;
//        FieldByName('id').AsInteger := strtoint(ResponseStr.Strings[0]);
        FieldByName('id').AsInteger := i;
        FieldByName('clientid').AsInteger := client_id;
        FieldByName('terminalid').AsInteger := terminal_id;

        FieldByName('qid').AsString := ResponseStr.Strings[1];
        FieldByName('filename').AsString := ResponseStr.Strings[2];
        FieldByName('newfilename').AsString := ResponseStr.Strings[8];

        FieldByName('filelocation').AsString := ResponseStr.Strings[9];

        FieldByName('showposition').AsInteger := strtoint(ResponseStr.Strings[3]);
        FieldByName('showtype').AsInteger := strtoint(ResponseStr.Strings[4]);

        FieldByName('startdate').AsDateTime := GenDateTime(ResponseStr.Strings[5]);
        FieldByName('stopdate').AsDateTime := GenDateTime(ResponseStr.Strings[6]);
        FieldByName('active').AsBoolean := ResponseStr.Strings[7] = 'Y';
        FieldByName('filehash').AsString := ResponseStr.Strings[10];
        FieldByName('filesize').AsInteger := strtoint(ResponseStr.Strings[11]);
        post;
        ShowLog('Finish: Write Banner to local DB = ' + ResponseStr.Strings[2],memo3);
        Application.ProcessMessages;
      except
        ShowLog('Error: Cannot write Banner to local DB = ' + ResponseStr.Strings[2],memo3);
      end;
    end;
  end;
end;

procedure TfrmLog.ManageQB1;
begin
  dm.qryClearPBQ1.ExecSQL;
  dm.RefreshPlayBQ;

  with dm.qryGetActiveBanner do
  begin
    if BannerMode = 1 then
    begin
      if active then close;
      Parameters.ParamByName('stype').Value := 1;
      Parameters.ParamByName('sposition').Value := 1;
      open;
    end
    else
    begin
      if active then close;
      Parameters.ParamByName('stype').Value := 2;
      Parameters.ParamByName('sposition').Value := 1;
      open;
    end;

    while not eof do
    begin
      dm.tblPlayBq1.Append;
      dm.tblPlayBq1.FieldByName('id').AsInteger := fieldbyname('id').AsInteger;
      dm.tblPlayBq1.Post;
      next;
    end;
  end;
end;

procedure TfrmLog.ManageQB2;
begin
  dm.qryClearPBQ2.ExecSQL;
  dm.RefreshPlayQ;

  with dm.qryGetActiveBanner do
  begin
    if BannerMode = 2 then
    begin
      if active then close;
      Parameters.ParamByName('stype').Value := 2;
      Parameters.ParamByName('sposition').Value := 2;
      open;
    end;

    while not eof do
    begin
      dm.tblPlayBq2.Append;
      dm.tblPlayBq2.FieldByName('id').AsInteger := fieldbyname('id').AsInteger;
      dm.tblPlayBq2.Post;
      next;
    end;
  end;
end;

procedure TfrmLog.GetBannerFiles;
var sFile,sName,sLoc,sHash,sLog,sHashFile: string;
  bDownload:boolean;
begin
  dm.RefreshPlayBQ;

  dm.tblPlaybq1.First;
  while not dm.tblPlaybq1.Eof do begin
    with dm.tblBanner do begin
      if Locate('id', dm.tblPlaybq1.Fields[0].AsInteger, []) then begin
        inc(iLoopDownload);
        sFile:=FieldByName('filename').AsString;
        sName:=FieldByName('newfilename').AsString;
        sLoc:=FieldByName('filelocation').AsString;
        sHash:=FieldByName('filehash').AsString;
        bDownload:=not checkFileComplete(sName,sFile);
{
        if FileExists(FullVideoDir + sName) then begin
          sHashFile:=getMD5CheckSum(FullVideoDir+sName);
          if (sHash = sHashFile) then begin
            bDownload:=false;
          end;
        end;
}
          // if file is not in client, download it.
//          GetBannerFile(FieldByName('newfilename').AsString, FieldByName('filelocation').AsString);

        if (bDownload) then begin
          sLog:='Start Download file : '+ sFile;
          showLog(sLog,mmoDown);
          DownloadFile(sFile,sName,sLoc,sHash,idHTTPDown1);
        end else begin
          showLog('File '+sFile+' Exist.',mmoDown);
        end;
        Application.ProcessMessages;
      end;
    end;

    dm.tblPlaybq1.Next;
  end;

  dm.tblPlaybq2.First;
  while not dm.tblPlaybq2.Eof do begin
    with dm.tblBanner do begin
      if Locate('id', dm.tblPlaybq2.Fields[0].AsInteger, []) then begin
        sFile:=FieldByName('filename').AsString;
        sName:=FieldByName('newfilename').AsString;
        sLoc:=FieldByName('filelocation').AsString;
        sHash:=FieldByName('filehash').AsString;
        bDownload:=true;

{
        if FileExists(FullVideoDir + sName) then begin
          sHashFile:=getMD5CheckSum(FullVideoDir+sName);
          if (sHash = sHashFile) then begin
            bDownload:=false;
          end;
        end;
}
          // if file is not in client, download it.
//          GetBannerFile(FieldByName('newfilename').AsString, FieldByName('filelocation').AsString);

        bDownload:=not checkFileComplete(sName,sFile);

        if (bDownload) then begin
          sLog:='Start Download file : '+ sFile;
          showLog(sLog,mmoDown);
          DownloadFile(sFile,sName,sLoc,sHash,idHTTPDown1);
        end else begin
          showLog('File '+sFile+' Exist.',mmoDown);
        end;
        Application.ProcessMessages;
      end;
    end;
    dm.tblPlaybq2.Next;
  end;
end;

procedure TfrmLog.GetBannerFile(fname, floc: string);
var sRequest: string;
begin
  try
    Application.ProcessMessages;
    MemoryStream.Clear;

    ShowLog('Start: Get Banner ' + dm.tblBanner.FieldByName('filename').AsString,mmoDown);
    sRequest:=floc+'?mc='+frmMain.txtSerial.Text;

//    IdHTTP1.get(floc, MemoryStream);
    IdHTTPDown1.get(sRequest, MemoryStream);
    Application.ProcessMessages;

    MemoryStream.Position := 0;
    MemoryStream.SaveToFile(FullVideoDir + fname);
    Application.ProcessMessages;

    ShowLog('Finish: Get Banner ' + dm.tblBanner.FieldByName('filename').AsString,mmoDown);

  except
    ShowLog('Error: Cannot get Banner ' + dm.tblBanner.FieldByName('filename').AsString,mmoDown);
  end;
end;

function TfrmLog.ExtractResponse(Input: string): TStringList;
var
  i: integer;
  Temp: string;

begin
  Output.Clear;
  Temp := '';

  for i := 1 to Length(Input) do
  begin
    if (Input[i] <> '|') and (i <> Length(Input)) then
    begin
      Temp := Temp + Input[i];
    end
    else
    begin
      if (Trim(Input[i]) <> '') and (Input[i] <> '|') then
        Temp := Temp + Input[i];
      Output.Add(Trim(Temp));
      Temp := '';
    end;
  end;

  result := Output;
end;

function TfrmLog.GenHTTPRequest(forvideo: boolean; cmd: integer; param1: string): string;
var sRequest:string;
begin
  if forvideo then
    sRequest := http_server + video_program + memo1.Lines[cmd]
  else
    sRequest := http_server + news_program + memo1.Lines[cmd];

  sRequest:=sRequest
      + param1 + '&terminalid=' + inttostr(Terminal_ID)
      + '&mac=' + edtMAC.Text
      + '&mc=' + frmMain.txtSerial.Text
      + '&va=' + cValAgent;
//      + '&sys_pwd=' + Client_PWD
//      + '&clientid=' + inttostr(Client_ID);
  result:=ReplaceSpace(sRequest);
end;

function TfrmLog.GenDateTime(input: string): TDateTime;
var
  yy, mm, dd, hh, min, ss: word;
  sinput: string;
begin
  sinput :=  trim(input);
  if length(sinput) = 19 then
  begin
    try
      yy := strtoint( sinput[1] + sinput[2] + sinput[3] + sinput[4] );
      mm := strtoint( sinput[6] + sinput[7] );
      dd := strtoint( sinput[9] + sinput[10] );

      hh :=  strtoint( sinput[12] + sinput[13] );
      min := strtoint( sinput[15] + sinput[16] );
      ss :=  strtoint( sinput[18] + sinput[19] );

      Result := EncodeDateTime(yy, mm, dd, hh, min, ss, 0);
    except
      result := Now;
    end;
  end
  else
    result := now;
end;

function TfrmLog.GetFileSize(fileName : wideString) : Int64;
 var
   sr : TSearchRec;
 begin
   if FindFirst(fileName, faAnyFile, sr ) = 0 then
      result := Int64(sr.FindData.nFileSizeHigh) shl Int64(32) + Int64(sr.FindData.nFileSizeLow)
   else
      result := -1;

   FindClose(sr) ;
 end;

//function TfrmLog.GetFileSize(fname: string): integer;
//var
//  myFile: file of Byte;
//begin
//  result := 0;
//  if fname <> '' then
//  begin
//    try
//      AssignFile(myFile, fname);
//      Reset(myFile);
//      result := FileSize(myFile);
//      CloseFile(myFile);
//    except
//      CloseFile(myFile);
//    end;
//  end;
//end;

//--------------------------------------------------------------------------------
// Manage Input
//--------------------------------------------------------------------------------
function TfrmLog.ZeroPad(input: string; cnt: integer): string;
var
  i: integer;
begin
  result := input;
  for i := length(input) to cnt - 1 do
  begin
    result := '0' + result;
  end;
end;

function TfrmLog.ReplaceSpace(input: string): string;
var
  i: integer;
begin
  result := '';
  for i := 1 to length(input) do
  begin
    if input[i] = ' ' then
    begin
      result := result + '%20'
    end
    else
    begin
      result := result + input[i]
    end;
  end;
end;

function TfrmLog.DateTimeForSQL(const dateTime : TDateTime): string;
begin
  result := FormatDateTime('#yyyy-mm-dd hh.nn.ss#', dateTime) ;
end;

//--------------------------------------------------------------------------------
// HTTP Event Control
//--------------------------------------------------------------------------------
procedure TfrmLog.IdHTTP1Work(Sender: TObject; AWorkMode: TWorkMode;
  const AWorkCount: Integer);
begin
  if AWorkMode = wmRead then
  begin
    ProgressBar1.Position := AWorkCount;
    Application.ProcessMessages;
  end;
end;

procedure TfrmLog.IdHTTP1WorkBegin(Sender: TObject; AWorkMode: TWorkMode;
  const AWorkCountMax: Integer);
begin
  if AWorkMode = wmRead then
  begin
    TrackLog('Config','HTTP Begin '+idHTTP1.Request.URL);
    ProgressBar1.Max := AWorkCountMax;
    ProgressBar1.Position := 0;
    Application.ProcessMessages;
  end;
end;

procedure TfrmLog.IdHTTP1WorkEnd(Sender: TObject; AWorkMode: TWorkMode);
begin
  TrackLog('Config','HTTP End');
  ProgressBar1.Position := 0;
  Application.ProcessMessages;
end;

procedure TfrmLog.btnHideClick(Sender: TObject);
begin
  if frmLog.Showing then frmLog.Hide;
end;

procedure TfrmLog.Hide1Click(Sender: TObject);
begin
  btnHide.Click;
end;

//--------------------------------------------------------------------------------
// Update Config to Server
//--------------------------------------------------------------------------------
procedure TfrmLog.GetClientConfig;
var
  i,iRun,iMaxField: integer;
  httpstr: string;
  sField,svalue,sSQL: string;
  iQID: integer;
begin
  if (Not IsConnected) then begin
    ShowLog('Error: No Intenet Connection',memo3);
    exit;
  end;

  with dm.tblConfig do
  begin
    if active then close;
    open;
    iMaxField:=46;
//    for i := 1 to Fields.Count - 1 do
    for i := 1 to iMaxField do begin
      if ((i mod 5) = 0) then begin

        TrackLog('Config','GetServerQ');
        GetServerQ;
        Application.ProcessMessages;

        TrackLog('Config','GetServerActiveQF');
        GetServerActiveQF;
        Application.ProcessMessages;

        TrackLog('Config','ManageQ');
        ManageQ;
        Application.ProcessMessages;
      end;

      if (lowercase(Fields[i].FieldName)= 'qid') then continue;
      sHTTP := GenHTTPRequest(true, 14, inttostr(client_id)
                               + '&attr=' + lowercase(Fields[i].FieldName));

      try
        Application.ProcessMessages;
        MemoryStream.Clear;
//  Insert by Boripat 05/09/2011
//        if (idHTTP1.Connected) then idHTTP1.Disconnect;
//  End Insert
        mmoDebug.Lines.Add(sHTTP);
        IdHTTP1.get(sHTTP, MemoryStream);
        Application.ProcessMessages;
        MemoryStream.Position := 0;
        Memo2.Clear;
        Memo2.Lines.LoadFromStream(MemoryStream);
        mmoDebug.Lines.Add(Memo2.Lines.Text);
        ResponseStr := ExtractResponse(Memo2.Lines[0]);
        sSQL:=Fields[i].FieldName;
        if (ResponseStr.Count > 2) then begin

          for iRun:=0 to memo2.Lines.Count-1 do begin
            ResponseStr := ExtractResponse(Memo2.Lines[iRun]);
            sField:=ResponseStr.Strings[0];
            iQID:=strtoint(ResponseStr.Strings[2]);
{   Check Found New QID :: Found ->Edit ; Not Found -> New Record}
            if (lowercase(Fields[i].FieldName) = 'clientid') then begin
              if (not locate('QID',iQID,[])) then begin
                append;
                FieldByName('QID').asInteger:=iQID;
                FieldByName('ClientID').asInteger:=Client_ID;
                post;
              end;
            end;

            refresh;
            sSQL:='UPDATE configs SET '+Fields[i].FieldName+' =';
            if Fields[i].DataType = ftWideString then begin
              sSQL:=sSQL+''''+ResponseStr.Strings[1]+'''';
            end else if Fields[i].DataType = ftInteger then begin
              sSQL:=sSQL+ResponseStr.Strings[1];
            end else if Fields[i].DataType = ftBoolean then begin
              if (LowerCase(ResponseStr.Strings[1])='true') then begin
                sSQL:=sSQL+'true';
              end else begin
                sSQL:=sSQL+'false';
              end;
            end;
            sSQL:=sSQL+' WHERE QID='+inttostr(iQID);
            dm.qry.SQL.Text:=sSQL;
            mmoDebug.Lines.Append(sSQL);
            dm.qry.ExecSQL;
{
            if (locate('QID',iQID,[])) then begin
              if (lowercase(sField) = lowercase(Fields[i].FieldName)) then begin
                edit;
                if Fields[i].DataType = ftWideString then begin
                  Fields[i].AsString := ResponseStr.Strings[1];
                end else if Fields[i].DataType = ftInteger then begin
                  try
                    Fields[i].AsInteger := strtoint(ResponseStr.Strings[1]);
                  except

                  end;
                end else if Fields[i].DataType = ftBoolean then begin
                  if LowerCase(ResponseStr.Strings[1]) = 'true' then
                    Fields[i].AsBoolean := true
                  else
                    Fields[i].AsBoolean := false;
                end;
                post;
              end;
            end;
}
          end;
          ShowLog('Finish: Get Configs from Server Client_id = '
                           + inttostr(client_id) + ' ' + Fields[i].FieldName,memo3);

        end else begin

          if lowercase(ResponseStr.Strings[0]) = lowercase(Fields[i].FieldName) then begin
            ShowLog('Finish: Get Configs from Server Client_id = '
                           + inttostr(client_id) + ' ' + Fields[i].FieldName,memo3);

            edit;
            if Fields[i].DataType = ftWideString then
            begin
              Fields[i].AsString := ResponseStr.Strings[1];
            end else if Fields[i].DataType = ftInteger then begin
              try
                Fields[i].AsInteger := strtoint(ResponseStr.Strings[1]);
              except

              end;
            end else if Fields[i].DataType = ftBoolean then begin
              if LowerCase(ResponseStr.Strings[1]) = 'true' then
                Fields[i].AsBoolean := true
              else
                Fields[i].AsBoolean := false;
            end;
            post;
          end else begin
          ShowLog('Error 1: Cannot Get Configs from Server Client_id = '
                                 + inttostr(client_id) + ' ' + Fields[i].FieldName,memo3);
          ShowLog('Send = "'+ httpstr+ '" return => ' + ResponseStr.Text,memo3);
          end;
        end;
//  Insert by Boripat 05/09/2011
//        if (idHTTP1.Connected) then idHTTP1.Disconnect;
//  End Insert
      except
        ShowLog('Error 2: Cannot Get Configs from Server Client_id = '
                               + inttostr(client_id) + ' ' + Fields[i].FieldName,memo3);
//  Insert by Boripat 05/09/2011
        if (idHTTP1.Connected) then idHTTP1.Disconnect;
//  End Insert
        abort;
      end;

    end; // end of for loop to loop all config fields
  end;
end;

procedure TfrmLog.PutClientOnline(sType:string; sCategory: string);
var
  httpstr: string;
  sLastOnline: string;
  sCurrentTime: string;
  myLogFile: TextFile;
  sFile,shttp:string;
begin
  if (Not IsConnected) then begin
    ShowLog('Error: No Intenet Connection',memo3);
    exit;
  end;

  sLastOnline:='2000-01-01 00:00:00';
  sFile:=sProgramDir+'log/'+sCategory+'.log';

  if (fileexists(sFile)) then begin
    sLastOnline:=checkModify(sType,sFile);
    end;
  sCurrentTime:=formatdatetime('yyyy-mm-dd hh:nn:ss',now);

  shttp := GenHTTPRequest(true, 16, inttostr(client_id)
                               + '&attr=' + sType + '_online'
                               + '&ctime=' + sCurrentTime
                               + '&ltime=' + sLastOnline);
  httpstr:= StringReplace(shttp, ' ', '%20',
                          [rfReplaceAll, rfIgnoreCase]);
  try
    Application.ProcessMessages;
    MemoryStream.Clear;
    IdHTTP1.get(httpstr, MemoryStream);
    Application.ProcessMessages;
    ShowLog('Finish: Put '+sType+' Online State to Server Client_id = '
                 + inttostr(client_id) + ' ' + sLastOnline,memo3);
  except
    ShowLog('Error: Cannot Put '+sType+' Online State to Server Client_id = '
                 + inttostr(client_id) + ' ' + sLastOnline,memo3);
  end;
end;

procedure TfrmLog.PutClientData(sKey,sValue: string);
var
  httpstr: string;
  sFile,shttp:string;
begin
  if (Not IsConnected) then begin
    ShowLog('Error: No Intenet Connection',memo3);
    exit;
  end;


  shttp := GenHTTPRequest(true, 17, inttostr(client_id)
                               + '&attr=' + sKey
                               + '&value=' + sValue);
  httpstr:= StringReplace(shttp, ' ', '%20',
                          [rfReplaceAll, rfIgnoreCase]);
  try
    Application.ProcessMessages;
    MemoryStream.Clear;
    TrackLog('Config',httpstr);
    IdHTTP1.get(httpstr, MemoryStream);
    Application.ProcessMessages;
    ShowLog('Finish: Put Data '+sKey+'='+sValue,memo3);
  except
    ShowLog('Error: Put Data '+sKey,memo3);
  end;
end;

procedure TfrmLog.PutClientConfig;
var
  i: integer;
  httpstr: string;
begin
  with dm.tblConfig do
  begin
    if active then close;
    open;

    for i := 1 to Fields.Count - 1 do
    begin
      httpstr := GenHTTPRequest(true, 13, inttostr(client_id)
                               + '&attr=' + Fields[i].FieldName
                               + '&value=' + Fields[i].AsString);
      try
        Application.ProcessMessages;
        MemoryStream.Clear;
        IdHTTP1.get(httpstr, MemoryStream);
        Application.ProcessMessages;
        ShowLog('Finish: Put Configs to Server Client_id = '
             + inttostr(client_id) + ' '
             + Fields[i].FieldName + '=' + Fields[i].AsString,memo3);
      except
        ShowLog('Error: Cannot Put Configs to Server Client_id = '
             + inttostr(client_id) + ' '
             + Fields[i].FieldName + '=' + Fields[i].AsString,memo3);
        abort;
      end;

    end; // end of for loop to loop all config fields
  end;
end;

procedure TfrmLog.UpdateConfigVariable;
var adoDB:TADOTable;
begin
  if (dm.tblPlayLayout.IsEmpty) then begin
    adoDB:=dm.tblConfig;
  end else begin
    adoDB:=dm.tblPlayLayout;
  end;

{    with dm.tblConfig do begin          }
   with adoDB do begin
      First;
      Client_ID := FieldByName('ClientID').AsInteger;
      Terminal_ID := FieldByName('TerminalID').AsInteger;
      Client_PWD := FieldByName('sys_pwd').AsString;

      Show_News := FieldByName('show_news').AsBoolean;
      Show_Ads := FieldByName('show_ads').AsBoolean;
      Video_dir := trim(FieldByName('video_directory').AsString);
      Def_video := trim(FieldByName('default_video').AsString);

      http_server := trim(FieldByName('http_server').AsString);
      video_program := trim(FieldByName('video_program').AsString);
      news_program := trim(FieldByName('news_program').AsString);

      refresh_time := FieldByName('refresh_time').AsInteger;
      ftp_server := trim(FieldByName('ftp_server').AsString);
      ftp_port := FieldByName('ftp_port').AsInteger;
      ftp_user := trim(FieldByName('ftp_user').AsString);
      ftp_pass := trim(FieldByName('ftp_pass').AsString);
      ftp_path := trim(FieldByName('ftp_path').AsString);
      ftp_passive := FieldByName('ftp_passive').AsBoolean;
      debug_mode := FieldByName('debug_mode').AsBoolean;

      BannerMode := FieldByName('banner_mode').AsInteger;
    end;
{
  end else begin
    with dm.tblPlayLayout do begin
      First;
      Client_ID := FieldByName('ClientID').AsInteger;
      Terminal_ID := FieldByName('TerminalID').AsInteger;
      Client_PWD := FieldByName('sys_pwd').AsString;

      Show_News := FieldByName('show_news').AsBoolean;
      Show_Ads := FieldByName('show_ads').AsBoolean;
      Video_dir := trim(FieldByName('video_directory').AsString);
      Def_video := trim(FieldByName('default_video').AsString);

      http_server := trim(FieldByName('http_server').AsString);
      video_program := trim(FieldByName('video_program').AsString);
      news_program := trim(FieldByName('news_program').AsString);

      refresh_time := FieldByName('refresh_time').AsInteger;
      ftp_server := trim(FieldByName('ftp_server').AsString);
      ftp_port := FieldByName('ftp_port').AsInteger;
      ftp_user := trim(FieldByName('ftp_user').AsString);
      ftp_pass := trim(FieldByName('ftp_pass').AsString);
      ftp_path := trim(FieldByName('ftp_path').AsString);
      ftp_passive := FieldByName('ftp_passive').AsBoolean;
      debug_mode := FieldByName('debug_mode').AsBoolean;

      BannerMode := FieldByName('banner_mode').AsInteger;
    end;
  end;
}
end;

procedure TfrmLog.TranslateDOW(input : string);
begin
  with dm.tblQ  do
  begin
    FieldByName('ismonday').AsBoolean := strtoint(input[1]) =1;
    FieldByName('istuesday').AsBoolean := strtoint(input[2]) =1;
    FieldByName('iswednesday').AsBoolean := strtoint(input[3]) =1;
    FieldByName('isthursday').AsBoolean := strtoint(input[4]) =1;
    FieldByName('isfriday').AsBoolean := strtoint(input[5]) =1;
    FieldByName('issaturday').AsBoolean := strtoint(input[6]) =1;
    FieldByName('issunday').AsBoolean := strtoint(input[7]) =1;
  end;
end;

function TfrmLog.GenTimeInterval(input: string): TDateTime;
var
  inputint, hh, mm : integer;
begin
  try
    inputint:=strtoint(input);
  except
    inputint := 0;
  end;
  hh:=inputint div 100;
  mm:=inputint mod 100;
  Result := Date+EncodeTime(hh,mm,0,0);
end;

procedure TfrmLog.Image3DblClick(Sender: TObject);
begin
  if (Not Timer1.Enabled) then
    Timer1.Enabled:=true;
  inc(iClick);

  if (iClick >=3) then
  begin
    tbDebug.TabVisible:= not tbDebug.TabVisible;
    iClick:=0;
    if (not tbDebug.TabVisible) then
      begin
      AdminPage.ActivePageIndex:=0;
      tbConfig.SetFocus;
      end
    Else
      begin
      AdminPage.ActivePageIndex:=1;
      Memo2.SetFocus;
      end;
  end;
end;

procedure TfrmLog.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=false;
  Timer1.Interval:=5000;
  iClick:=0;
end;

procedure TfrmLog.gbtnMinimizeClick(Sender: TObject);
begin
  if frmLog.Showing then frmLog.Hide;
end;

procedure TfrmLog.SaveLog(sCategory: string; sValue:string);
var
  sName,sDir,sFile,sNew,sError : string;
  myLogFile: TextFile;
  iRun:integer;
begin
  sFile:=sProgramDir+'log/'+sCategory+'.log';
  try
    if (not fileexists(sFile)) then begin
      AssignFile(myLogFile,sFile);
      Rewrite(myLogFile);
      CloseFile(myLogFile);
    end;

    iRun:=0;
    while ((FileInUse(sFile)) and (iRun < 100)) do begin
      sleep(random(100)+100);
      inc(iRun);
    end;

    if (not fileinuse(sFile)) then begin
      AssignFile(myLogFile,sFile);
//    ReWrite(myLogFile);
      Append(myLogFile);
      WriteLn(myLogFile, sValue);
      CloseFile(myLogFile);
    end;
  Except
    on E : Exception do begin
      sError:='Exception on Function = SaveLog('+sCategory+','+sValue+')';
      sError:=sError+#13#10+'Exception Class = '+E.ClassName;
      sError:=sError+#13#10+'Exception Message = '+E.Message;
      ShowMessage(sError);
    end;
  end;
end;

procedure TfrmLog.TrackLog(sType,sValue:string);
var
  sFile,sError:string;
  myLogFile: TextFile;
  iRun: integer;
begin
  sFile:=sProgramDir+'log/'+sTrackLog+'_'
      +formatdatetime('yymmdd',now)+'_'+sType+'.log';
  if (bTrack) then
    begin
    try
      if (not fileexists(sFile)) then begin
        AssignFile(myLogFile,sFile);
        Rewrite(myLogFile);
        CloseFile(myLogFile);
      end;
      iRun:=0;
      while ((FileInUse(sFile)) and (iRun < 100)) do begin
        sleep(random(100)+100);
        inc(iRun);
      end;
      if (not fileinuse(sFile)) then begin
        AssignFile(myLogFile,sFile);
  //    ReWrite(myLogFile);
        Append(myLogFile);
        WriteLn(myLogFile, formatdatetime('yy/mm/dd hh:nn:ss',now())+ '# '+sValue);
        CloseFile(myLogFile);
      end;
    Except
      on E : Exception do begin
        sError:='Exception on Function = TrackLog('+sType+','+sValue+')';
        sError:=sError+#13#10+'Exception Class = '+E.ClassName;
        sError:=sError+#13#10+'Exception Message = '+E.Message;
        ShowMessage(sError);
      end;
    end;
  end;
end;

procedure TfrmLog.timRecordTimer(Sender: TObject);
begin
  SaveLog(sAGNLog,formatdatetime('yyyy-mm-dd hh:nn:ss',now()));
end;

function TfrmLog.FileTimeToDTime(FTime: TFileTime): TDateTime;
var
  LocalFTime: TFileTime;
  STime: TSystemTime;
begin
  FileTimeToLocalFileTime(FTime, LocalFTime);
  FileTimeToSystemTime(LocalFTime, STime);
  Result := SystemTimeToDateTime(STime);
end;

function TfrmLog.checkModify(sType:string; sRecord: string):string;
var
  dtCurrent: TDateTime;
  dtLast: TDateTime;
  Day,Time: TStringList;
  Input: TStringList;
  myLogFile:TextFile;
  iDiff:integer;
  sFile,sValue,sNow : string;
  SR: TSearchRec;
begin
  result := formatdatetime('yyyy-mm-dd hh:nn:ss',now);
  sFile:=sProgramDir+'log/'+sRecord+'.log';
//  sFile:=sProgramDir+sRecord;
{
  while (fileinuse(sFile)) do sleep(500);
  if (fileexists(SFile)) then
    begin
    AssignFile(myLogFile,sFile);
    Reset(myLogFile);
    ReadLn(myLogFile, sValue);
    CloseFile(myLogFile);
    end;
}
  Input:=TStringList.Create;
  Day:=TStringList.Create;
  Time:=TStringList.Create;
  if FindFirst(sFile, faAnyFile, SR) = 0 then
    begin
    dtLast := FileTimeToDTime(SR.FindData.ftLastWriteTime);
//    dtLast := FileTimeToDTime(SR.FindData.ftLastAccessTime);
    end
  Else
    begin
    sValue:='2013-01-01 00:00:00';
    Split(' ',sValue,Input);
    Split('-',Input[0],Day);
    Split(':',Input[1],Time);
    dtLast:=EncodeDateTime(strtoInt(Day[0]),strtoint(Day[1]),strtoint(Day[2]),
      strtoint(Time[0]),strtoint(Time[1]),strtoint(Time[2]),0);
    end;

  result := formatdatetime('yyyy-mm-dd hh:nn:ss',dtLast);
  Day.Free;
  Time.Free;
  Input.Free;
end;

procedure TfrmLog.DownloadConfig();
var sInput,sFullScreen: string;
begin
// Insert By Boripat 05/09/2011
//  InProcess := True;
  if (not InProcess) then
  begin
// End Insert
  InProcess := True;
  TrackLog('Config','Start Connection');
  try
    frmLog.Memo3.Clear;
    frmMain.Timer1.Enabled:=false;
    TrackLog('Config','GetClientConfig');
    GetClientConfig;
    TrackLog('Config','UpdateConfigVariable');
    UpdateConfigVariable;
    Application.ProcessMessages;

    TrackLog('Config','GetServerQ');
    GetServerQ;
    Application.ProcessMessages;

    if Show_News then
      begin
      TrackLog('Config','GetServerNews');
      GetServerNews;
      end;
    Application.ProcessMessages;

    if Show_Ads then
    begin
      TrackLog('Config','GetServerBanner');
      GetServerBanner;
      Application.ProcessMessages;

      if BannerMode = 1 then
      begin
        TrackLog('Config','ManageQB1');
        ManageQB1;
        Application.ProcessMessages;
      end
      else
      begin
        TrackLog('Config','ManageQB1');
        ManageQB1;
        Application.ProcessMessages;

        TrackLog('Config','ManageQB2');
        ManageQB2;
        Application.ProcessMessages;
      end
    end;
{
    TrackLog('GetBannerFiles');
    GetBannerFiles;
    Application.ProcessMessages;
}
    TrackLog('Config','GetServerActiveQF');
    GetServerActiveQF;
    Application.ProcessMessages;

    TrackLog('Config','ManageQ');
    ManageQ;
    Application.ProcessMessages;

{
//    GetAllVideoes;
    TrackLog('GetVideoFiles');
    GetVideoFiles;
    Application.ProcessMessages;

    GetAllVideos;
    Application.ProcessMessages;
}
    if dm.tblConfig.State = dsedit then
      dm.tblConfig.Post;

    TrackLog('Config','putClientOnline');

    putClientOnline('Player',sSMPLog);
    Application.ProcessMessages;

    putClientOnline('Agent',sAgnLog);
    Application.ProcessMessages;

    putClientOnline('Monitor',sMonLog);
    Application.ProcessMessages;

    sInput:=inttostr(screen.Width)+':'+inttostr(screen.Height);
    putClientData('screen_res',sInput);
    Application.ProcessMessages;

    frmMain.ReadConfig();
    sInput:=inttostr(iPlayerTop);
    sInput:=sInput+':'+inttostr(iPlayerLeft);
    sInput:=sInput+':'+inttostr(iPlayerWidth);
    sInput:=sInput+':'+inttostr(iPlayerHeight);
    putClientData('program_res',sInput);
    Application.ProcessMessages;
    sFullScreen:='0';
    if (bFullScreen) then sFullScreen:='1';
    putClientData('fullscreen',sFullScreen);
    Application.ProcessMessages;

    putClientData('version_agent',sValAgent);
    Application.ProcessMessages;
    putClientData('version_mon',sValMonitor);
    Application.ProcessMessages;
    putClientData('version_player',sValPlayer);
    Application.ProcessMessages;

    GetHTMLConfig();
    getHTMLURL();
    frmMain.CreateURLConfig();

  finally
    InProcess := False;
    frmMain.Timer1.Interval:=1000*60*refresh_time;
    frmMain.Timer1.Enabled:=true;
  end;
// Insert By Boripat 05/09/2011
  end;
// End Insert
end;

procedure TfrmLog.btnConnectClick(Sender: TObject);
begin
  GetAllData();
end;

procedure TfrmLog.btnRefreshClick(Sender: TObject);
begin
  GetAllData();
end;

procedure TfrmLog.DownloadFiles();
begin
  if (not bDownloadProcess) then begin
    mmoDown.Clear;
    bDownloadProcess:=true;
    btnDownRefresh.Enabled:=false;
    btnDownStop.Enabled:=true;
    TrackLog('Download','GetBannerFiles');
    GetBannerFiles;

    TrackLog('Download','GetVideoFiles');
    GetVideoFiles;

    TrackLog('Download','GetAllVideoFiles');
    GetAllVideos;

    bDownloadProcess:=false;
    btnDownRefresh.Enabled:=true;
    btnDownStop.Enabled:=false;
  end else
    Application.ProcessMessages;

end;

procedure TfrmLog.btnDownRefreshClick(Sender: TObject);
begin
  DownloadFiles();
end;

procedure TfrmLog.IdHTTPDown1Work(Sender: TObject; AWorkMode: TWorkMode;
  const AWorkCount: Integer);
begin
  if AWorkMode = wmRead then
  begin
    barDown1.Position := AWorkCount;
    lblDown1.Caption:= format('Now Download %n KB/ %n KB',[barDown1.Position/1024,barDown1.Max/1024]);
    Application.ProcessMessages;
  end;

end;



procedure TfrmLog.IdHTTPDown1WorkBegin(Sender: TObject;
  AWorkMode: TWorkMode; const AWorkCountMax: Integer);
begin
  if AWorkMode = wmRead then
  begin
    TrackLog('Download','HTTP Begin '+idHTTPDown1.Request.URL);
    barDown1.Max := AWorkCountMax;
    barDown1.Position := 0;
    Application.ProcessMessages;
  end;

end;

procedure TfrmLog.IdHTTPDown1WorkEnd(Sender: TObject;
  AWorkMode: TWorkMode);
begin
  TrackLog('Download','HTTP End');
  BarDown1.Position := 0;
  Application.ProcessMessages;
  bDownTrack1:=false;
  lblDown1.Caption:='';
end;

procedure TfrmLog.IdHTTPLogWorkBegin(Sender: TObject;
  AWorkMode: TWorkMode; const AWorkCountMax: Integer);
begin
  if AWorkMode = wmRead then
  begin
    TrackLog('Download2','HTTP Begin '+idHTTPLog.Request.URL);
    barDown2.Max := AWorkCountMax;
    barDown2.Position := 0;
    Application.ProcessMessages;
  end;
end;

procedure TfrmLog.IdHTTPLogWork(Sender: TObject; AWorkMode: TWorkMode;
  const AWorkCount: Integer);
begin
  if AWorkMode = wmRead then
  begin
    barDown2.Position := AWorkCount;
    Application.ProcessMessages;
  end;

end;

procedure TfrmLog.IdHTTPLogWorkEnd(Sender: TObject;
  AWorkMode: TWorkMode);
begin
  TrackLog('Download2','HTTP End');
  BarDown2.Position := 0;
  Application.ProcessMessages;
  bDownTrack2:=false;
end;

procedure TFrmLog.DownloadStop();
begin
  if (idHTTPDown1.Connected) then begin
//    idHTTPDown1.ClearWriteBuffer;
//    idHttpDown1.CancelWriteBuffer;
    idHTTPDown1.Disconnect;
  end;
end;

procedure TfrmLog.btnDownStopClick(Sender: TObject);
begin
  DownloadStop();
end;


procedure TfrmLog.FormResize(Sender: TObject);
begin
  frmLog.Left:=screen.WorkAreaWidth-frmLog.Width;
  frmLog.Top:=screen.WorkAreaHeight-frmLog.Height;
end;

procedure TfrmLog.timChkDownTimer(Sender: TObject);
var iSlow,iDrop,iCount:integer;
begin
  iCount:=60;
  iSlow:=5;
  iDrop:=45;
  if (bDownloadProcess) then begin
    if (iLastDownloadPos=barDown1.Position) then begin
      inc(iDownloadCounter);
      if (iDownloadCounter > ceil(iCount*iSlow/iCount)) then
        barDown1.BarColor:=RGB(255,255,0);
      if (iDownloadCounter > ceil(iCount*iDrop/iCount)) then
        barDown1.BarColor:=RGB(255,69,0);
    end else begin
      iDownloadCounter:=0;
      barDown1.BarColor:=RGB(0,0,127);
    end;
    if (iDownloadCounter > iCount) then begin
//      btnDownStop.Click;
      DownloadStop();
      timChkDown.Enabled:=false;
    end;
    iLastDownloadPos:=barDown1.Position;
    end;
end;

function TfrmLog.GetFileDate(sFileName,sType: string):string;
var
  dtCurrent: TDateTime;
  dtReturn: TDateTime;
  Day,Time: TStringList;
  Input: TStringList;
  myLogFile:TextFile;
  iDiff:integer;
  sFile,sValue,sNow : string;
  SR: TSearchRec;
begin
  result:= formatdatetime('dd/mm/yyyy hh:nn:ss',now);
//  sFile:=sProgramDir+sRecord;
{
  while (fileinuse(sFile)) do sleep(500);
  if (fileexists(SFile)) then
    begin
    AssignFile(myLogFile,sFile);
    Reset(myLogFile);
    ReadLn(myLogFile, sValue);
    CloseFile(myLogFile);
    end;
}
  Input:=TStringList.Create;
  Day:=TStringList.Create;
  Time:=TStringList.Create;
  if FindFirst(sFileName, faAnyFile, SR) = 0 then begin
    if (sType = 'access') then begin
      dtReturn := FileTimeToDTime(SR.FindData.ftLastAccessTime);
    end else if (sType = 'create') then begin
      dtReturn := FileTimeToDTime(SR.FindData.ftCreationTime);
    end else begin
      dtReturn := FileTimeToDTime(SR.FindData.ftLastWriteTime);
//    dtLast := FileTimeToDTime(SR.FindData.ftLastAccessTime);
    end;
  end
  Else begin
    sValue:=formatdatetime('yyyy-mm-dd hh:nn:ss',Now());
    Split(' ',sValue,Input);
    Split('-',Input[0],Day);
    Split(':',Input[1],Time);
    dtReturn:=EncodeDateTime(strtoInt(Day[0]),strtoint(Day[1]),strtoint(Day[2]),
      strtoint(Time[0]),strtoint(Time[1]),strtoint(Time[2]),0);
  end;

//  result:= formatdatetime('yyyy-mm-dd hh:nn:ss',dtReturn);
  result:= formatdatetime('dd/mm/yyyy hh:nn:ss',dtReturn);
  Day.Free;
  Time.Free;
  Input.Free;
end;

function TfrmLog.CheckFileComplete(sFileName,sOrgName:string): boolean;
var
  sFile,sCDate,sADate,sUDate,sFDate,sLog:string;
  iSize,iFileSize:INT64;
begin
  result:=false;
  sFile:=FullVideoDir+sFileName;
  TrackLog('Download',inttostr(iLoopDownload)+' Check File '+sFileName);
  if (not FileExists(sFile)) then begin
    TrackLog('Download',inttostr(iLoopDownload)+'No Local File '+sFileName);
    exit;
  end;

  iSize:=getFileSize(sFile);
  sCDate:=GetFileDate(sFile,'create');
  sADate:=GetFileDate(SFile,'access');
  sUDate:=GetFileDate(SFile,'update');
//  sHash:=getMD5CheckSum(sFile);
  with dm.tblFileList do begin
    if active then close;
    open;
    first;
    if (locate('fname',sFileName,[])) then begin
// Found File Name in DB
//      sFDate:=formatdatetime('yyyy-mm-dd hh:nn:ss',fieldbyname('fupdate').asdatetime);
      sFDate:=formatdatetime('dd/mm/yyyy hh:nn:ss',fieldbyname('fupdate').asdatetime);
      iFileSize:=fieldbyName('fsize').AsInteger;
      sLog:=inttostr(iLoopDownload)+' "'+sFileName+'" Detail '
//        + chr(13)+ 'DB: Size='+inttostr(iFileSize)+ ' Time: '+sFDate
//        + chr(13)+ 'Local: Size='+inttostr(iSize) + ' Time: '+sCDate;
        + '( DB Size : '+inttostr(iFileSize)+' / '
        + ' Disk Size : '+inttostr(iSize)+' )';
      TrackLog('Download',sLog);
//      if ((sCDate <> sFDate) or (iFileSize <> iSize)) then begin
      if (iFileSize <> iSize) then begin
        TrackLog('Download',inttostr(iLoopDownload)+' File "'+sFileName+'" Size not match.');
        exit;
      end else begin
        TrackLog('Download',inttostr(iLoopDownload)+' Exist File "'+sFileName+'" ');
      end;
      result:=true;
    end else begin
      append;
      fieldbyname('forg').AsString:=sOrgName;
      fieldbyname('fname').AsString:=sFileName;
      fieldbyname('fsize').AsInteger:=iSize;
      fieldbyname('fupdate').AsDateTime:=strtodatetime(sCDate);
      fieldbyname('faccess').AsDateTime:=strtodatetime(sADate);
      post;
      sLog:='"'+sFileName+'" '
        +'DB: Size='+inttostr(iSize) + ' Time: '+sCDate;
      TrackLog('Download',inttostr(iLoopDownload)+' Update DB with Exist File'+sLog);
      result:=true;
    end;
  end;
end;

procedure TfrmLog.AddFileComplete(sName,sFileName:string);
var
  sFile,sCDate,sADate,sUDate,sFDate,sLog:string;
  iSize,iFileSize:INT64;
begin
  sFile:=FullVideoDir+sFileName;
  if (not FileExists(sFile)) then exit;

  iSize:=getFileSize(sFile);
  sCDate:=GetFileDate(SFile,'create');
  sADate:=GetFileDate(SFile,'access');
  sUDate:=GetFileDate(SFile,'update');

  with dm.tblFileList do begin
    if active then close;
    open;
    first;
    if (locate('fname',sFileName,[])) then begin
// Found File Name in DB
//      sFDate:=formatdatetime('yyyy-mm-dd hh:nn:ss',fieldbyname('fupdate').asdatetime);
      sFDate:=formatdatetime('dd/mm/yyyy hh:nn:ss',fieldbyname('fupdate').asdatetime);
      iFileSize:=fieldbyName('fsize').AsInteger;
//      if ((sCDate = sFDate) and (iFileSize = iSize)) then begin
      if (iFileSize = iSize) then begin
        sLog:='Same as Old in Database not update database';
        TrackLog('Download',sLog);
//        exit;
      end;
      Edit;
      fieldbyName('fsize').AsInteger:=iSize;
      fieldbyName('fupdate').asdatetime:=strtodatetime(sCDate);
      Post;
      sLog:='Update Data to DB last:: Date: '+sFDate+' Size: '+inttostr(iFileSize)
        + ' New:: Date: '+ sCDate+ ' Size: '+inttostr(iSize);
      TrackLog('Download',sLog);
    end else begin
      append;
      fieldbyName('forg').AsString:=sName;
      fieldbyName('fname').AsString:=sFileName;
      fieldbyName('fsize').AsInteger:=iSize;
      fieldbyName('fupdate').asdatetime:=strtodatetime(sCDate);
      fieldbyName('faccess').asdatetime:=strtodatetime(sCDate);
      post;
      sLog:='Add New Data to DB New:: Date: '+ sCDate+ ' Size: '+inttostr(iSize);
      TrackLog('Download',sLog);
    // Found File Name in DB
    end;
  end;
end;

procedure TfrmLog.SendPlayLog();
var sRequest,sHTTP:string;
  bDelete:boolean;
  iRun: integer;
begin
  if (not InProcess) then begin
// End Insert
    InProcess := True;
    TrackLog('Config','Start Connection');

    with dm.tblPlayLog do begin
      if active then close;
      open;
      FIRST;
      iRun:=0;
      while ((not EOF) and (iRun<=cMaxSendLog)) do begin
        inc(iRun);
        bDelete:=TRUE;
        sRequest := http_server + video_program;
        sRequest:=sRequest+ '?cmd=put_status'
          + '&terminalid=' + inttostr(Terminal_ID)
          + '&mac=' + edtMAC.Text
          + '&mc=' + frmMain.txtSerial.Text
          + '&va=' + cValAgent
//          + '&sys_pwd=' + Client_PWD
          + '&clientid=' + inttostr(Client_ID)
          + '&attr=local_log'
          + '&value='+FieldByName('file').AsString
          + '&type='+FieldByName('Type').AsString
          + '&time='+FieldByName('cdate').AsString
          + '&action='+FieldByName('action').AsString
          + '&qid='+inttostr(FieldByName('qid').AsInteger)
          + '&clog='+inttostr(FieldByName('log_id').AsInteger);
        try
          sHTTP:=ReplaceSpace(sRequest);
          Application.ProcessMessages;
          MemoryStream.Clear;
          IdHTTP1.get(sHTTP, MemoryStream);
          Application.ProcessMessages;
          ShowLog('Finish: Put PlayStatus PlayID='
                   + inttostr(fieldbyname('ID').AsInteger),memo3);
          delete;
//          post;
        except
          bDelete:=FALSE;
          ShowLog('Error: Put PlayStatus PlayID='
                   + inttostr(fieldbyname('ID').AsInteger),memo3);
        end;
        if (bDelete) then begin
          First;
        end else begin
          Next;
        end;
      end;
    end;
    InProcess:=false;
  end;
end;

procedure TfrmLog.GetAllData();
begin
  if not InProcess then begin
//    frmLog.btnConnect.Click;
      DownloadConfig();
      GetHTMLConfig();
      SendPlayLog();
    if not bDownloadProcess then begin
//      frmLog.btnDownRefresh.Click;
        DownloadFiles();
    end;
  end;
end;

procedure TfrmLog.GetHTMLConfig();
var iRun:integer;
  shttp:string;
begin

  iHTMLItem:=5;
  iHTMLSize:=50;
  sHTMLDirection:='left';
  bHTMLShow:=true;
  iHTMLInterval:=60;
{
  try
    Application.ProcessMessages;
    MemoryStream.Clear;
    shttp := GenHTTPRequest(true, 25, inttostr(client_id)
      + '&attr=get' + '&qid=0');
    IdHTTP1.get(shttp, MemoryStream);
    Application.ProcessMessages;
    MemoryStream.Position := 0;
    Memo2.Clear;
    Memo2.Lines.LoadFromStream(MemoryStream);
    if (Length(Memo2.Lines[0])>5) then begin
      ResponseStr := ExtractResponse(Memo2.Lines[iRun]);
      bHTMLShow:=strtobool(ResponseStr.Strings[0]);
      iHTMLInterval:=strtoint(ResponseStr.Strings[1]);
      sHTMLDirection:=lowercase(ResponseStr.Strings[2]);
      iHTMLSize:=strtoint(ResponseStr.Strings[3]);
    end;
  except
    on E : Exception do begin
      sError:='Exception on Function = GetHTMLConfig';
      sError:=sError+#13#10+'Exception Class = '+E.ClassName;
      sError:=sError+#13#10+'Exception Message = '+E.Message;
      ShowMessage(sError);
    end;
  end;
}
  ShowLog('Finish: Get Configs from Server Client_id = '
                           + inttostr(client_id) + ' html_config',memo3);
end;

procedure TFrmLog.GetHTMLURL();
var iRun:integer;
  shttp:string;
begin
  try
    Application.ProcessMessages;
    MemoryStream.Clear;
    shttp := GenHTTPRequest(true, 18, inttostr(client_id)
      +'&qid=0');

    IdHTTP1.get(shttp, MemoryStream);
    Application.ProcessMessages;
    MemoryStream.Position := 0;
    Memo2.Clear;
    Memo2.Lines.LoadFromStream(MemoryStream);
    if (Memo2.Lines.Count > 1) then begin
      iHTMLItem:=memo2.Lines.Count-1;
      setLength(arrURL,iHTMLItem+1);
      dm.qry.SQL.Text:='DELETE FROM html_url';
      dm.qry.ExecSQL;
      with dm.tblHTMLUrl do begin
        if active then close;
        open;
      end;
      for iRun:=1 to iHTMLItem do begin
        ResponseStr := ExtractResponse(Memo2.Lines[iRun]);
//  id,qid,refreshtime,url,ordering


        arrURL[iRun]:=ResponseStr.Strings[3];
        with dm.tblHTMLUrl do begin
          append;
          fieldbyName('id').AsInteger:=strtoint(ResponseStr.Strings[0]);
          fieldbyName('cid').AsInteger:=strtoint(ResponseStr.Strings[1]);
          fieldbyName('qid').AsInteger:=strtoint(ResponseStr.Strings[2]);
          fieldbyName('interval').AsInteger:=strtoint(ResponseStr.Strings[3]);
          fieldbyName('url').AsString:=ResponseStr.Strings[4];
          fieldbyName('ordering').AsInteger:=strtoint(ResponseStr.Strings[5]);
          post;
        end;

      end;
    end else begin
      iHTMLItem:=5;
      setLength(arrURL,iHTMLItem+1);
      arrURL[1]:='http://192.168.1.41/module/test0/';
      arrURL[2]:='http://192.168.1.41/module/test1/';
      arrURL[3]:='http://192.168.1.41/module/test2/';
      arrURL[4]:='http://www.signageexpress.com/';
      arrURL[5]:='http://www.revenue-express.com/';
    end;
  except
    on E : Exception do begin
      sError:='Exception on Function = GetHTMLURL';
      sError:=sError+#13#10+'Exception Class = '+E.ClassName;
      sError:=sError+#13#10+'Exception Message = '+E.Message;
      ShowMessage(sError);
    end;
  end;
{

}
  ShowLog('Finish: Get Configs from Server Client_id = '
                           + inttostr(client_id) + ' html_url',memo3);
end;


procedure TfrmLog.btnDebugClearClick(Sender: TObject);
begin
  mmoDebug.Clear;
end;

procedure TfrmLog.GetNewClientConfig;
var
  iLine,iRun:integer;
  sField,svalue: string;

begin
  if (Not IsConnected) then begin
    ShowLog('Error: No Intenet Connection',memo3);
    exit;
  end;

  with dm.tblConfig do
  begin
    if active then close;
    open;
    Application.ProcessMessages;
    MemoryStream.Clear;
    sHTTP := GenHTTPRequest(true, 18, inttostr(client_id)
      +'&qid=0');

//    dm.qry.SQL.Clear;
//    dm.qry.SQL.Text:='DELETE FROM configs';
//    dm.qry.ExecSQL;

    IdHTTP1.get(sHTTP, MemoryStream);
    mmoDebug.Lines.Add(sHTTP);
    Application.ProcessMessages;
    MemoryStream.Position := 0;
    Memo2.Clear;
    Memo2.Lines.LoadFromStream(MemoryStream);
    mmoDebug.Lines.Add(Memo2.Lines.Text);
    iLine:=Memo2.Lines.Count;
    for iRun:=1 to iLine do begin
      ResponseStr := ExtractResponse(Memo2.Lines[iRun]);
{
  0->ID, 1->QID, 2->CID, 3->debug_mode,
  4-> ftp_server, 5-> ftp_port, 6->ftp_path,
  7->ftp_user, 8->ftp_pass, 9->ftp_passive
  10->http_server,
  11->banner_mode, 12->banner_pos, 13->banner_prop, 14->banner_show,
  15->banner_show_interval, 16->banner_width,
  17->clock_show, 18->clock_format, 19->clock_font_color, 20->clock_bgcolor
  21->def_file, 22->html_show, 23->html_pos, 24->html_interval, 25->html_size,
  26->logo_ratio, 27->logo_stretch, 28->logo_width,
  29->news_show, 30->news_pos, 31->news_step, 32->news_delay,
  33->news_font_name, 34->news_font_color, 35->news_font_size,
  36->news_font_bold, 37->news_font_italic, 38->news_font_underline,
  39->news_program, 40->rss_active, 41->rss_url, 42->rss_interval,43->video_source
}
      refresh;
      try
        try
          append;
          FieldbyName('ID').AsInteger:=strtoint(ResponseStr.Strings[0]);
          FieldbyName('QID').AsInteger:=strtoint(ResponseStr.Strings[1]);
          FieldbyName('ClientID').AsInteger:=strtoint(ResponseStr.Strings[2]);
          FieldbyName('debug_mode').AsBoolean:=strtobool(ResponseStr.Strings[3]);
          FieldbyName('ftp_server').AsString:=trim(ResponseStr.Strings[4]);
          FieldbyName('ftp_port').AsInteger:=strtoint(ResponseStr.Strings[5]);
          FieldbyName('ftp_path').AsString:=trim(ResponseStr.Strings[6]);
          FieldbyName('ftp_user').AsString:=trim(ResponseStr.Strings[7]);
          FieldbyName('ftp_pass').AsString:=trim(ResponseStr.Strings[8]);
          FieldbyName('ftp_passive').AsBoolean:=strtobool(ResponseStr.Strings[9]);
          FieldbyName('http_server').AsString:=trim(ResponseStr.Strings[10]);

          FieldbyName('banner_mode').AsInteger:=strtoint(ResponseStr.Strings[11]);
//          FieldbyName('banner_pos').AsString:=lowercase(trim(ResponseStr.Strings[11]));
          FieldbyName('banner_prop').AsBoolean:=strtobool(ResponseStr.Strings[13]);
//          FieldbyName('banner_show').AsBoolean:=strtobool(ResponseStr.Strings[14]);
          FieldbyName('show_ads').AsBoolean:=strtobool(ResponseStr.Strings[14]);
          FieldbyName('banner_show_interval').AsInteger:=strtoint(ResponseStr.Strings[15]);
          FieldbyName('banner_width').AsInteger:=strtoint(ResponseStr.Strings[16]);
//          FieldbyName('banner_size').AsInteger:=strtoint(ResponseStr.Strings[16]);
          FieldbyName('show_clock').AsBoolean:=strtobool(ResponseStr.Strings[17]);
//          FieldbyName('clock_show').AsBoolean:=strtobool(ResponseStr.Strings[17]);
//          FieldbyName('clock_format').AsString:=trim(ResponseStr.Strings[18]);
//          FieldbyName('clock_font_color').AsString:=trim(ResponseStr.Strings[19]);
//          FieldbyName('clock_bgcolor').AsString:=trim(ResponseStr.Strings[20]);
          FieldbyName('def_file').AsString:=trim(ResponseStr.Strings[21]);
          FieldbyName('html_show').AsBoolean:=strtobool(ResponseStr.Strings[22]);
          FieldbyName('html_pos').AsString:=lowercase(trim(ResponseStr.Strings[23]));
          FieldbyName('html_interval').AsInteger:=strtoint(ResponseStr.Strings[24]);
          FieldbyName('html_size').AsInteger:=strtoint(ResponseStr.Strings[25]);

          FieldbyName('logo_ratio').AsBoolean:=strtobool(ResponseStr.Strings[26]);
          FieldbyName('logo_stretch').AsBoolean:=strtobool(ResponseStr.Strings[27]);
          FieldbyName('logo_width').AsInteger:=strtoint(ResponseStr.Strings[28]);

          FieldbyName('show_news').AsBoolean:=strtobool(ResponseStr.Strings[29]);
//          FieldbyName('news_show').AsBoolean:=strtobool(ResponseStr.Strings[29]);
//          FieldbyName('news_pos').AsString:=lowercase(trim(ResponseStr.Strings[30]));
          FieldbyName('news_step').AsInteger:=strtoint(ResponseStr.Strings[31]);
          FieldbyName('news_delay').AsInteger:=strtoint(ResponseStr.Strings[32]);
          FieldbyName('news_font_name').AsString:=trim(ResponseStr.Strings[33]);
          FieldbyName('news_font_color').AsInteger:=strtoint(ResponseStr.Strings[34]);
          FieldbyName('news_font_size').AsInteger:=strtoint(ResponseStr.Strings[35]);
          FieldbyName('news_font_bold').AsBoolean:=strtobool(ResponseStr.Strings[36]);
          FieldbyName('news_font_italic').AsBoolean:=strtobool(ResponseStr.Strings[37]);
          FieldbyName('news_font_underline').AsBoolean:=strtobool(ResponseStr.Strings[38]);
          FieldbyName('news_program').AsString:=trim(ResponseStr.Strings[39]);
// -->         FieldbyName('news_back_color').AsString:=trim(ResponseStr.Strings[33]);

          FieldbyName('rss_active').AsBoolean:=strtobool(ResponseStr.Strings[40]);
          FieldbyName('rss_url').AsString:=trim(ResponseStr.Strings[41]);
          FieldbyName('rss_interval').AsInteger:=strtoint(ResponseStr.Strings[42]);


          FieldbyName('video_source').AsString:=trim(ResponseStr.Strings[43]);
          post;
        except
        ShowLog('Error: Get Queue Configs from Server; QID = '
                           + ResponseStr.Strings[1],memo3);
        end;
      finally
        ShowLog('Finish: Get Queue Configs from Server; QID = '
                           + ResponseStr.Strings[1],memo3);
      end;

    end;
  end;
end;

end.
